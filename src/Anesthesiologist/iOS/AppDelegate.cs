﻿using Anesthesiologist.DAL;

using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using ObjCRuntime;
using UIKit;

namespace Anesthesiologist.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();
            RoundedBoxView.Forms.Plugin.iOSUnified.RoundedBoxViewRenderer.Init();

            UIApplication.SharedApplication.SetMinimumBackgroundFetchInterval(UIApplication.BackgroundFetchIntervalMinimum);

            // Code for starting up the Xamarin Test Cloud Agent
#if ENABLE_TEST_CLOUD
            Xamarin.Calabash.Start();
#endif

			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}

        public override async void PerformFetch(UIApplication application, Action<UIBackgroundFetchResult> completionHandler)
        {
            try
            {
                var provider = await DALFactory.GetActiveConfigurationAsync();
                if (provider != null)
                {
                    using (var dal = await DALFactory.GetProviderAsync(provider))
                    {
                        var patients = await IDataStorage.GetAllPatientsForProvider(provider.Id);
                        var updated = await dal.RefreshPatientsAsync(patients);
                        if (updated)
                        {
                            // TODO: Notify??
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // TODO: Log or report exception
            }

            base.PerformFetch(application, completionHandler);
        }
    }
}
