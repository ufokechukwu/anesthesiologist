﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Anesthesiologist.Models;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Anesthesiologist.DAL.Mock
{
    public class MockDataProvider : IDataProvider
    {
        private List<Patient> _patients = new List<Patient>();
        private Dictionary<string, Patient> _encounters = new Dictionary<string, Patient>();

        public bool IsInitialized { get; set; }

        public async Task InitializeAsync(IDataProviderConfig config, INavigation navigation = null)
        {
            _patients.Clear();

            _patients.Add(new Patient()
            {
                ProviderConfigId = config.Id,
                ProviderID = "12345",
                MRN = "12345",
                NameFamily = "Smith",
                NamesGiven = new List<string> { "John" },
                NamePrefix = "Mr"
            });

            _encounters.Add("24680", _patients[0]);

            IsInitialized = true;
        }

        public async Task<Patient> GetPatientByProviderIdAsync(string id)
        {
            return _patients.Where(p => p.ProviderID == id).FirstOrDefault();
        }

        public async Task<Patient> GetPatientByFINAsync(string fin)
        {
            Patient p;
            _encounters.TryGetValue(fin, out p);
            return p;
        }

        public async Task<Patient> GetPatientByMRNAsync(string mrn)
        {
            return _patients.Where(p => p.MRN == mrn).FirstOrDefault();
        }

        public async Task<IEnumerable<Patient>> GetPatientByNameAsync(string name, FHIR.FHIRSearchType searchCriteria)
        {
            return null;            //TODO
        }

        public async Task<IEnumerable<Patient>> FindPatientsAsync(string familyName, string givenName)
        {
            if (string.IsNullOrEmpty(familyName))
            {
                if (string.IsNullOrEmpty(givenName))
                {
                    return null;
                }
                else
                {
                    return _patients.Where(p => String.Join(" ", p.NamesGiven).ToLowerInvariant().Contains(givenName.ToLowerInvariant()));
                }
            }
            else
            {
                if (string.IsNullOrEmpty(givenName))
                {
                    return _patients.Where(p => p.NameFamily.Equals(familyName, StringComparison.CurrentCultureIgnoreCase));
                }
                else
                {
                    return _patients.Where(p => p.NameFamily.Equals(familyName, StringComparison.CurrentCultureIgnoreCase))
                                    .Where(p => String.Join(" ", p.NamesGiven).ToLowerInvariant().Contains(givenName.ToLowerInvariant()));
                }
            }
        }

        public async Task<bool> RefreshPatientAsync(Patient patient, bool force = false)
        {
            return false;
        }

        public async Task<bool> RefreshPatientsAsync(IEnumerable<Patient> patients, bool force = false)
        {
            return false;
        }

        public void Dispose() { }
    }
}
