﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anesthesiologist.DAL.Mock
{
    public class MockDataProviderConfig : IDataProviderConfig
    {
        public string Id { get; set; } = Guid.Empty.ToString();
        public string Name {
            get { return "Mock Data Provider"; }
            set { }
        }
        public string Description {
            get { return "MOCK"; }
        }

        public event EventHandler ShouldSave;

        public string UUID { get { return Id + Name; } }

        public bool IsActiveConfig { get; set; }

        public FHIRAuthType AuthType { get; set; } = FHIRAuthType.None;

        public void SaveChanges() { }

        public IDataProvider CreateInstance()
        {
            var provider = new MockDataProvider();
            return provider;
        }
    }
}
