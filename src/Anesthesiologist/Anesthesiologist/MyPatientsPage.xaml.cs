﻿using Anesthesiologist.DAL;
using Anesthesiologist.DAL.FHIR;
using Anesthesiologist.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Anesthesiologist
{
	public partial class MyPatientsPage : ContentPage
	{
		public MyPatientsPage()
		{
			InitializeComponent();

            Title = "My Patients";
            PlatformConfigName.Text = "Please Select or Activate A FHIR Server!";

            LoadAllPatientData();

            PatientsListXAML.ItemSelected += OnItemSelected;
            
            PatientsListXAML.Refreshing += async (s, e) =>
            {
                var config = await DALFactory.GetActiveConfigurationAsync();
                if (config != null)
                {
                    var patients = await IDataStorage.GetAllPatientsForProvider(config.UUID);
                    using (var dal = await DALFactory.GetProviderAsync(config, Navigation))
                    {
                        foreach (var patient in patients)
                        {
                            if (await dal.RefreshPatientAsync(patient, true))
                            {
                                await IDataStorage.SavePatient(patient, config.UUID);
                            }
                        }
                    }
                }
                PatientsListXAML.EndRefresh();
            };

            MessagingCenter.Subscribe<PatientInfoPage>(this, "reloadMyPatientsPageData", (sender) =>
            {
                LoadAllPatientData();
            });
        }

        async void OnRefresh(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            var patient = mi.CommandParameter as Patient;
            if (patient == null) return;

            PatientsListXAML.IsRefreshing = true;

            var config = await DALFactory.GetConfigurationAsync(patient.ProviderConfigId);
            if (config != null)
            {
                using (var dal = await DALFactory.GetProviderAsync(config, Navigation))
                {
                    if (await dal.RefreshPatientAsync(patient, true))
                    {
                        await IDataStorage.SavePatient(patient, config.UUID);
                    }
                }
            }

            PatientsListXAML.IsRefreshing = false;
        }

        async void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            var patient = mi.CommandParameter as Patient;
            if (patient == null) return;

            if (await DisplayAlert("Confirm Delete", $"Are you sure you wish to remove {patient.NameFull}?", "Yes", "No"))
            {

                var config = await DALFactory.GetConfigurationAsync(patient.ProviderConfigId);
                if (config != null)
                {
                    await IDataStorage.DeletePatient(patient, config.UUID);
                    LoadAllPatientData();
                }
            }
        }

        async void LoadAllPatientData()
        {
            var config = await DALFactory.GetActiveConfigurationAsync();
            if (config != null)
            {
                //PlatformConfigName.Text = config.Name + " (ACTIVE)";
                PlatformConfigName.IsVisible = false;
                PatientsListXAML.ItemsSource = null;
                PatientsListXAML.ItemsSource = await IDataStorage.GetAllPatientsForProvider(config.UUID);
            }
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var patient = PatientsListXAML.SelectedItem as Patient;
            PatientsListXAML.SelectedItem = null;
            if (patient != null)
            {
                var detailPage = new PatientInfoPage(patient);
                await Navigation.PushAsync(detailPage);
            }
        }
    }
}
