﻿using Anesthesiologist.DAL;
using Anesthesiologist.DAL.FHIR;
using Anesthesiologist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Anesthesiologist
{
	public partial class PatientInfoPage : TabbedPage
	{
        Patient _patient = null;

		public PatientInfoPage(Patient patient)
		{
			InitializeComponent();

            _patient = patient;
            this.BindingContext = patient;

            Title = $"Patient: {patient.NameFull}";
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (BindingContext == null) return;

            patientTestsGrid.RowDefinitions.Clear();
            patientTestsGrid.Children.Clear();

            foreach (var testKey in _patient.SerumTests.Keys)
            {
                var testName = testKey.ToString();

                var test = _patient.SerumTests[testKey].FirstOrDefault();

                if (test != null)
                {
                    AddGridRow(patientTestsGrid, testName, $"{test.Value} {test.Unit} ({test.EffectiveDate.ToString("yyyy-M-d h:mm tt")})");
                }
            }
        }

        async void OnRefresh(object sender, EventArgs e)
        {
            var config = await DALFactory.GetConfigurationAsync(_patient.ProviderConfigId);
            if (config != null)
            {
                using (var dal = await DALFactory.GetProviderAsync(config, Navigation))
                {
                    if (await dal.RefreshPatientAsync(_patient, true))
                    {
                        this.BindingContext = null;
                        this.BindingContext = _patient;
                        await IDataStorage.SavePatient(_patient, config.UUID);
                    }
                }
            }
        }

        async void OnDelete(object sender, EventArgs e)
        {
            if (await DisplayAlert("Confirm Delete", "Are you sure you wish to delete this patient?", "Yes", "No"))
            {
                var config = await DALFactory.GetConfigurationAsync(_patient.ProviderConfigId);
                if (config != null)
                {
                    await IDataStorage.DeletePatient(_patient, config.UUID);

                    MessagingCenter.Send<PatientInfoPage>(this, "reloadMyPatientsPageData");

                    await Navigation.PopAsync();
                }
            }
        }


        private void AddGridRow(Grid grid, string header, object text)
        {
            grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            var rownum = grid.RowDefinitions.Count - 1;
            grid.Children.Add(new Label()
            {
                Text = header,
                TextColor = Color.Blue,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                FontAttributes = FontAttributes.Bold
            }, 0, rownum);
            grid.Children.Add(new Label()
            {
                Text = text != null ? text.ToString() : "",
                TextColor = Color.Blue,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label))
            }, 1, rownum);
        }


    }
}
