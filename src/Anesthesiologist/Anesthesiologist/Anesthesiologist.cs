﻿using Akavache;
using System;

using Xamarin.Forms;

namespace Anesthesiologist
{
	public class App : Application
	{
        public App()
		{
            BlobCache.ApplicationName = "Anesthesiologist";

            // The root page of your application
            MainPage = new Anesthesiologist.NavigationRootPage();
		}

        protected override void OnStart()
		{
            // Handle when your app starts
        }

        protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
            // Handle when your app resumes
        }
    }
}
