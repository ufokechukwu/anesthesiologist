﻿using System;

using Xamarin.Forms;

namespace Anesthesiologist
{

	public class NavigationRootPage : Xamarin.Forms.MasterDetailPage
	{
		Page CurrentPage;

		public NavigationRootPage()
		{
			var menuPage = new MenuPage();

			menuPage.MenuList.ItemSelected += (sender, e) =>
			{

				NavigateTo(e.SelectedItem as SideMenuItem);
			};


			this.Master = menuPage;
			this.Detail = new NavigationPage(new MyPatientsPage());
		}

		void NavigateTo(SideMenuItem menu)
		{
			Page displayPage = (Page)Activator.CreateInstance(menu.TargetType);

			if (CurrentPage != null || CurrentPage != displayPage)
			{
				CurrentPage = displayPage;

				Detail = new NavigationPage(displayPage);
				IsPresented = false;
			}
			else {
				Detail = Detail;
				IsPresented = false;
			}

		}
	}
}

