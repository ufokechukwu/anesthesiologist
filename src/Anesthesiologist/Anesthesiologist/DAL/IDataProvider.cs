﻿using Anesthesiologist.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Anesthesiologist.DAL
{
    public interface IDataProvider : IDisposable
    {
        Task InitializeAsync(IDataProviderConfig options, INavigation navigation);
        bool IsInitialized { get; }

        Task<Patient> GetPatientByProviderIdAsync(string id);
        Task<Patient> GetPatientByFINAsync(string fin);
        Task<Patient> GetPatientByMRNAsync(string mrn);
        Task<IEnumerable<Patient>> GetPatientByNameAsync(string name, FHIR.FHIRSearchType searchCriteria);
        Task<IEnumerable<Patient>> FindPatientsAsync(string familyName, string givenName);

        Task<bool> RefreshPatientAsync(Patient patient, bool force = false);
        Task<bool> RefreshPatientsAsync(IEnumerable<Patient> patients, bool force = false);
    }
}
