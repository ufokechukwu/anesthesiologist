﻿using Akavache;
using Anesthesiologist.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace Anesthesiologist.DAL
{
    public class IDataStorage
    {
        public static async Task AddFhirSession(string providerID)
        {
            AllFHIRSessions allFhirSessions = await GetAllFHIRSessions();
            // Create the Session
            FHIRSession fhir_session = new FHIRSession(new List<Patient>(), providerID);
            if (allFhirSessions != null)
            {
                var session = allFhirSessions.Sessions.Find(item => item.ProviderConfigID == fhir_session.ProviderConfigID);
                if (session == null)
                {
                    allFhirSessions.Sessions.Add(fhir_session);
                    await SaveAllFHIRSessions(allFhirSessions);

                }
            }else
            {
                List<FHIRSession> sessions = new List<FHIRSession>();
                sessions.Add(fhir_session);
                allFhirSessions = new AllFHIRSessions(sessions);
                await SaveAllFHIRSessions(allFhirSessions);
            }
        }

        public static async Task DeleteFhirSession(string providerID)
        {
            AllFHIRSessions allFhirSessions = await GetAllFHIRSessions();
            FHIRSession fhir_session = new FHIRSession(new List<Patient>(), providerID);
            if (allFhirSessions != null)
            {
                var session = allFhirSessions.Sessions.Find(item => item.ProviderConfigID == fhir_session.ProviderConfigID);
                if (session != null)
                {
                    allFhirSessions.Sessions.Remove(session);
                    await SaveAllFHIRSessions(allFhirSessions);
                }
            }
        }

        public static async Task<List<Patient>> GetAllPatientsForProvider(string activeProviderID)
        {
            AllFHIRSessions allFhirSessions = await GetAllFHIRSessions();
            
            if (allFhirSessions != null)
            {
                var session = allFhirSessions.Sessions.Find(item => item.ProviderConfigID == activeProviderID);
                if (session != null)
                {
                    return session.Patients;
                }
            }

            return new List<Patient>();

        }


        public static async Task SavePatient(Patient patient, string providerID)
        {
            AllFHIRSessions allFhirSessions = await GetAllFHIRSessions();
            if (allFhirSessions != null)
            {
                var session = allFhirSessions.Sessions.Find(item => item.ProviderConfigID == providerID);
                if (session != null)
                {
                    var index = allFhirSessions.Sessions.IndexOf(session);
                    var find_patient = session.Patients.Find(item => item.MRN == patient.MRN);

                    if (find_patient == null)
                    {
                        allFhirSessions.Sessions[index].Patients.Add(patient);
                    }
                    else
                    {
                        var patient_index = allFhirSessions.Sessions[index].Patients.IndexOf(find_patient);
                        allFhirSessions.Sessions[index].Patients[patient_index] = patient;
                    }

                    /*
                    if (!session.Patients.Contains(patient))
                    {
                        session.Patients.Add(patient);
                        allFhirSessions.Sessions[index].Patients.Add(patient);
                    }
                    */

                    await SaveAllFHIRSessions(allFhirSessions);
                }
                else
                {
                    List<Patient> patient_list = new List<Patient>();
                    patient_list.Add(patient);
                    FHIRSession fhir_session = new FHIRSession(patient_list, providerID);
                    allFhirSessions.Sessions.Add(fhir_session);
                    await SaveAllFHIRSessions(allFhirSessions);
                }
            }
            
        }

        public static async Task DeletePatient(Patient patient, string providerID)
        {
            AllFHIRSessions allFhirSessions = await GetAllFHIRSessions();
            if (allFhirSessions != null)
            {
                var session = allFhirSessions.Sessions.Find(item => item.ProviderConfigID == providerID);
                if (session != null)
                {
                    var index = allFhirSessions.Sessions.IndexOf(session);

                    var find_patient = session.Patients.Find(item => item.MRN == patient.MRN);
                    allFhirSessions.Sessions[index].Patients.Remove(find_patient);
                    await SaveAllFHIRSessions(allFhirSessions);
                }
            }
        }

        public static async Task<AllFHIRSessions> GetAllFHIRSessions()
        {

            try
            {
               
                AllFHIRSessions allFhirSessions = new AllFHIRSessions();
                
                allFhirSessions = await BlobCache.LocalMachine.GetObject<AllFHIRSessions>("allfhirsessions".ToLower());
               
                return allFhirSessions;
            }
            catch (Exception ex)
            {
                // do something useful
                return new AllFHIRSessions();
            }
        }


        public static async Task SaveAllFHIRSessions(AllFHIRSessions allFHIRSessions)
        {
            try
            {
                await BlobCache.LocalMachine.InsertObject("allfhirsessions".ToLower(), allFHIRSessions);
            }
            catch (Exception ex)
            {
                // do something useful
            }
            
        }


    }
}
