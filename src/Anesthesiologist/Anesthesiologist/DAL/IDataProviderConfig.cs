﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anesthesiologist.DAL
{
    public interface IDataProviderConfig
    {
        string Id { get; set; }
        string Name { get; set; }
        string Description { get; }
        bool IsActiveConfig { get; set; }
        string UUID { get; }
        IDataProvider CreateInstance();
        void SaveChanges();
        event EventHandler ShouldSave;
    }

    public enum FHIRAuthType
    {
        None,
        Basic,
        SMART
    }
}
