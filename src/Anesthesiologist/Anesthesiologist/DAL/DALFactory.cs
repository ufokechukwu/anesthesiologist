﻿using Akavache;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Anesthesiologist.DAL
{
    public static class DALFactory
    {
        private const string INDEX_NAME = "ProviderIndex";
        private static List<Provider> _index;
        private static Dictionary<string, IDataProviderConfig> _configurations = new Dictionary<string, IDataProviderConfig>();
        private static Dictionary<string, IDataProvider> _providers = new Dictionary<string, IDataProvider>();

        private static bool _allConfigurationsLoaded = false;

        static DALFactory()
        {
            _index = BlobCache.Secure.GetOrCreateObject<List<Provider>>(
                INDEX_NAME,
                () => { return new List<Provider>(); }
            ).Wait();
        }

        public static async Task<IEnumerable<IDataProviderConfig>> GetAllConfigurationsAsync()
        {
            if (_allConfigurationsLoaded) return _configurations.Values;

            var configurations = new List<IDataProviderConfig>();
            //configurations.AddRange(await GetAllConfigurationsAsync<Mock.MockDataProviderConfig>());
            configurations.AddRange(await GetAllConfigurationsAsync<FHIR.FHIRDataProviderConfig>());
            return configurations;
        }

        public static async Task<IEnumerable<T>> GetAllConfigurationsAsync<T>() where T : IDataProviderConfig
        {
            // Silly me, thinking Akavache would work here...
            //return await BlobCache.Secure.GetAllObjects<T>();

            var configurations = new List<T>();

            foreach (var idx in _index.Where(i => i.TypeName == typeof(T).Name))
            {
                var config = await GetConfigurationAsync(idx.Id);
                if (config != null)
                {
                    configurations.Add((T)config);
                }
            }
            return configurations;
        }

        public static async Task<IDataProviderConfig> GetConfigurationAsync(string id)
        {
            if (_configurations.ContainsKey(id))
            {
                return _configurations[id];
            }
            else
            {
                IDataProviderConfig config = null;

                try
                {
                    var idx = _index.FirstOrDefault(p => p.Id == id);
                    if (idx != null)
                    {
                        if (idx.TypeName == typeof(FHIR.FHIRDataProviderConfig).Name)
                        {
                            config = await BlobCache.Secure.GetObject<FHIR.FHIRDataProviderConfig>(id);
                        }
                        //else if (idx.TypeName == typeof(Mock.MockDataProviderConfig).Name)
                        //{
                        //    config = await BlobCache.Secure.GetObject<Mock.MockDataProviderConfig>(id);
                        //}
                    }
                }
                catch { }

                if (config != null)
                {
                    _configurations.Add(config.Id, config);

                    config.ShouldSave += async (s, e) =>
                    {
                        await BlobCache.Secure.InsertObject(config.Id, config);
                    };

                    _allConfigurationsLoaded = _configurations.Count == _index.Count;
                }

                return config;
            }
        }

        public static async Task AddConfigurationAsync(IDataProviderConfig config)
        {
            if (!_index.Any(p => p.Id == config.Id))
            {
                _index.Add(new Provider { Id = config.Id, Name = config.Name, TypeName = config.GetType().Name });
                await BlobCache.Secure.InsertObject(INDEX_NAME, _index);
            }

            if (!_configurations.ContainsKey(config.Id))
            {
                _configurations.Add(config.Id, config);

                config.ShouldSave += async (s, e) =>
                {
                    await BlobCache.Secure.InsertObject(config.Id, config);
                };
            }

            await BlobCache.Secure.InsertObject(config.Id, config);
        }

        public static async Task RemoveConfigurationAsync(IDataProviderConfig config)
        {
            _index.RemoveAll(p => p.Id == config.Id);
            await BlobCache.Secure.InsertObject(INDEX_NAME, _index);
            _configurations.Remove(config.Id);
            _providers.Remove(config.Id);
            await BlobCache.Secure.Invalidate(config.Id);
        }

        public static async Task RemoveAllConfigurationsAsync()
        {
            foreach (var p in _index)
            {
                await BlobCache.Secure.Invalidate(p.Id);
            }
            await BlobCache.Secure.Invalidate(INDEX_NAME);

            _index.Clear();
            _providers.Clear();
        }

        public static void SetActiveConfiguration(IDataProviderConfig config)
        {
            foreach (var cfg in _configurations.Values)
            {
                if (cfg.IsActiveConfig)
                {
                    cfg.IsActiveConfig = false;
                    cfg.SaveChanges();
                }
            }
            config.IsActiveConfig = true;
            config.SaveChanges();
        }

        public static async Task<IDataProviderConfig> GetActiveConfigurationAsync()
        {
            var configurations = await GetAllConfigurationsAsync();
            return configurations.FirstOrDefault(cfg => cfg.IsActiveConfig);
        }

        /// <summary>
        /// Get the active IDataProvider.
        /// </summary>
        /// <param name="navigation">If supplied, used to present OAuth authorization dialog. If null, then will fail if OAuth authorization is required.</param>
        public static async Task<IDataProvider> GetProviderAsync(INavigation navigation = null)
        {
            var config = await GetActiveConfigurationAsync();
            return await GetProviderAsync(config, navigation);
        }

        /// <summary>
        /// Get the specified IDataProvider.
        /// </summary>
        /// <param name="id">Id of the configuration for the desired provider.</param>
        /// <param name="navigation">If supplied, used to present OAuth authorization dialog. If null, then will fail if OAuth authorization is required.</param>
        public static async Task<IDataProvider> GetProviderAsync(string id, INavigation navigation = null)
        {
            if (!_providers.ContainsKey(id))
            {
                return _providers[id];
            }
            else
            {
                var config = await GetConfigurationAsync(id);
                if (config != null)
                {
                    return await GetProviderAsync(config, navigation);
                }
            }

            return null;
        }

        /// <summary>
        /// Get the specified IDataProvider.
        /// </summary>
        /// <param name="config">The configuration for the desired provider.</param>
        /// <param name="navigation">If supplied, used to present OAuth authorization dialog. If null, then will fail if OAuth authorization is required.</param>
        public static async Task<IDataProvider> GetProviderAsync(IDataProviderConfig config, INavigation navigation = null)
        {
            IDataProvider provider = null;

            if (!_providers.ContainsKey(config.Id))
            {
                _providers[config.Id] = config.CreateInstance();
            }
            provider = _providers[config.Id];

            if (!provider.IsInitialized)
            {
                await provider.InitializeAsync(config, navigation);
            }
            return provider;
        }

        public class Provider
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string TypeName { get; set; }
        }

    }
}
