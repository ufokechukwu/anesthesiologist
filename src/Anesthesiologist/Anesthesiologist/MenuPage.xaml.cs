﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Anesthesiologist
{
	public partial class MenuPage : ContentPage
	{
		public ListView MenuList { get; set; }

		public MenuPage()
		{
			InitializeComponent();

			Title = "Menu";


			BackgroundColor = Color.FromHex("000000");

			var itemsDataSource = new List<SideMenuItem>() {
				new SideMenuItem("My Patients", "my_patients.png" , typeof(MyPatientsPage)),
				new SideMenuItem("New Patient", "new_patient.png" , typeof(NewPatientsPage)),
				new SideMenuItem("Settings", "my_settings.png" , typeof(SettingsPage)),
				new SideMenuItem("App Info", "app_info.png" , typeof(AppInfoPage))
			};

			MenuList = MenuListXAML;


			MenuListXAML.ItemsSource = itemsDataSource;

		}
	}

	public class SideMenuItem
	{
		public string Name { get; set; }

		public string IconSource { get; set; }

		public Type TargetType { get; set; }

		public SideMenuItem(string name, string iconSource, Type targetPageType)
		{
			Name = name;
			IconSource = iconSource;
			TargetType = targetPageType;
		}
	}
}
