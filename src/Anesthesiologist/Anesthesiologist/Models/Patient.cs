﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anesthesiologist.Models
{
    public class Patient : ModelBase
    {
        public const int UPDATE_INTERVAL = 5; // in minutes

        /////////////////////////////////////////////////////////////////////
        #region Static
        /////////////////////////////////////////////////////////////////////

        public static string[] HeightLoincCodes { get; private set; }
        public static string[] WeightLoincCodes { get; private set; }
        public static Dictionary<SerumTestTypes, string[]> SerumLoincMap { get; private set; }

        static Patient()
        {
            HeightLoincCodes = new string [] { "8302-2", "3137-7" };
            WeightLoincCodes = new string[] { "29463-7", "3141-9" };

            SerumLoincMap = new Dictionary<SerumTestTypes, string[]>();
            SerumLoincMap.Add(SerumTestTypes.Hematocrit, new string[] { "4544-3" });
            SerumLoincMap.Add(SerumTestTypes.Hemoglobin, new string[] { "718-7" });
            SerumLoincMap.Add(SerumTestTypes.PlateletCount, new string[] { "777-3" });
            SerumLoincMap.Add(SerumTestTypes.Sodium, new string[] { "2951-2" });
            SerumLoincMap.Add(SerumTestTypes.Potassium, new string[] { "2823-3" });
            SerumLoincMap.Add(SerumTestTypes.TCO2, new string[] { "2028-9" });
            SerumLoincMap.Add(SerumTestTypes.Chloride, new string[] { "2075-0" });
            SerumLoincMap.Add(SerumTestTypes.BUN, new string[] { "3094-0" });
            SerumLoincMap.Add(SerumTestTypes.Creatinine, new string[] { "2160-0" });
            SerumLoincMap.Add(SerumTestTypes.Glucose, new string[] { "2345-7" });
            SerumLoincMap.Add(SerumTestTypes.PT, new string[] { "5902-2" });
            SerumLoincMap.Add(SerumTestTypes.PTT, new string[] { "3173-2" });
            SerumLoincMap.Add(SerumTestTypes.INR, new string[] { "6301-6" });
            SerumLoincMap.Add(SerumTestTypes.Albumin, new string[] { "1751-7" });
            SerumLoincMap.Add(SerumTestTypes.AST_ALT, new string[] { "1916-6" });
            SerumLoincMap.Add(SerumTestTypes.Troponin, new string[] { "10839-9" });
            SerumLoincMap.Add(SerumTestTypes.BNP, new string[] { "30934-4" });
            SerumLoincMap.Add(SerumTestTypes.CK, new string[] { "2157-6" });
            SerumLoincMap.Add(SerumTestTypes.CKMB, new string[] { "32673-6" });
        }

        /////////////////////////////////////////////////////////////////////
        #endregion
        /////////////////////////////////////////////////////////////////////

        public Patient()
        {
            foreach (var test in SerumLoincMap.Keys)
            {
                SerumTests.Add(test, new List<SerumTest>());
            }
        }

        /////////////////////////////////////////////////////////////////////
        #region App Properties
        /////////////////////////////////////////////////////////////////////

        /// <summary>
        /// ID of the ProviderConfig instance used when retrieving this patient.
        /// (i.e., the server from whence it came)
        /// </summary>
        public string ProviderConfigId { get; set; }

        public bool IsActive { get; set; } = true;

        public DateTimeOffset CreatedAt { get; set; } = DateTime.Now;

        public DateTimeOffset LastUpdated { get; set; } = DateTime.MinValue;

        public DateTimeOffset LastRefreshed { get; set; } = DateTime.MinValue;

        public bool ShouldRefresh
        {
            get
            {
                return IsActive && LastRefreshed.AddMinutes(UPDATE_INTERVAL) < DateTime.Now;
            }
        }

        /////////////////////////////////////////////////////////////////////
        #endregion
        /////////////////////////////////////////////////////////////////////

        
        /////////////////////////////////////////////////////////////////////
        #region Core Customer Properties
        /////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Provider-supplied ID. May be the same as MRN.
        /// </summary>
        public string ProviderID { get; set; }

        public string MRN { get; set; }

        public string NameFamily { get; set; }

        public List<string> NamesGiven { get; set; } = new List<string>();

        public string NamePrefix { get; set; }

        public string NameSuffix { get; set; }

        public string NameFull
        {
            get
            {
                return string.Join(" ", NamesGiven) + " " + NameFamily;
            }
        }

        public string NameShort
        {
            get
            {
                return NamesGiven.FirstOrDefault() + " " + NameFamily;
            }
        }

        public string NameInitials
        {
            get
            {
                return NamesGiven.FirstOrDefault()?.Substring(0, 1) + NameFamily?.Substring(0, 1);
            }
        }

        public Genders Gender { get; set; } = Genders.Unknown;

        public DateTime BirthDate { get; set; } = DateTime.MinValue;

        public int Age
        {
            get
            {
                if (BirthDate == DateTime.MinValue)
                {
                    return -1;
                }

                DateTime today = DateTime.Today;
                int age = today.Year - BirthDate.Year;

                if (BirthDate > today.AddYears(-age))
                {
                    age--;
                }

                return age;
            }
        }

        public string Location { get; set; }

        public string Description1
        {
            get
            {
                return Age + " year-old " + Gender;
            }
        }

        public string Description2
        {
            get
            {
                return "Added: " + CreatedAt.ToString();
            }
        }


        /////////////////////////////////////////////////////////////////////
        #endregion
        /////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////
        #region Observations
        /////////////////////////////////////////////////////////////////////


        // 8302-2	Body height
        // 3137-7	Body height Measured
        public double Height { get; set; }

        public HeightUnits HeightUnit { get; set; }

        public string HeightFull
        {
            get
            {
                return $"{Height} {HeightUnit}";
            }
        }

        public double HeightInInches
        {
            get
            {
                if (HeightUnit == HeightUnits.Centimeters)
                {
                    return Height / 2.54;
                }
                return Height;
            }
        }

        public double HeightInCentimeters
        {
            get
            {
                if (HeightUnit == HeightUnits.Inches)
                {
                    return Height * 2.54;
                }
                return Height;
            }
        }

        // 29463-7	Body weight
        // 3141-9	Body weight Measured
        public double Weight { get; set; }

        public WeightUnits WeightUnit { get; set; }

        public string WeightFull
        {
            get
            {
                return $"{Weight} {WeightUnit}";
            }
        }

        public double WeightInPounds
        {
            get
            {
                if (WeightUnit == WeightUnits.Kilograms)
                {
                    return Weight / 0.4536;
                }
                return Weight;
            }
        }

        public double WeightInKilograms
        {
            get
            {
                if (WeightUnit == WeightUnits.Pounds)
                {
                    return Weight * 0.4536;
                }
                return Weight;
            }
        }

        // 39156-5	Body mass index(BMI) [Ratio]
        // Just calculate?
        //      BMI = Weight in kg / Height in meters
        public double BMI
        {
            get
            {
                if (Height != 0)
                {
                    return 100 * WeightInKilograms / HeightInCentimeters;
                }
                return 0;
            }
        }


        // http://www.mdcalc.com/creatinine-clearance-cockcroft-gault-equation/#about-equation

        // Estimating Creatinine Clearance(ml/min)
        //    Cockcroft and Gault equation:
        //        CrCl = (140 - age) x IBW / (Scr x 72)        (x 0.85 for females)

        // Estimate Ideal body weight in (kg)
        //    Males: IBW = 50 kg + 2.3 kg for each inch over 5 feet.
        //    Females: IBW = 45.5 kg + 2.3 kg for each inch over 5 feet.

        public double CreatinineClearance
        {
            get
            {
                if (Creatinine != null && Age > 0 && Height >= 60 && Weight > 0 && Gender != Genders.Unknown)
                {
                    var ibw = 2.3 * HeightInInches - 60;
                    ibw += Gender == Genders.Female ? 45.5 : 50;
                    var crcl = (140 - Age) * ibw / (Creatinine.Value * 72) ;
                    if (Gender == Genders.Female)
                        crcl *= 0.85;
                    return crcl;
                }
                return 0;
            }
        }

        public Dictionary<SerumTestTypes, List<SerumTest>> SerumTests { get; set; } = new Dictionary<SerumTestTypes, List<SerumTest>>();

        public SerumTest Hematocrit { get { return SerumTests[SerumTestTypes.Hematocrit].LastOrDefault(); } }
        public SerumTest Hemoglobin { get { return SerumTests[SerumTestTypes.Hemoglobin].LastOrDefault(); } }
        public SerumTest PlateletCount { get { return SerumTests[SerumTestTypes.PlateletCount].LastOrDefault(); } }
        public SerumTest Sodium { get { return SerumTests[SerumTestTypes.Sodium].LastOrDefault(); } }
        public SerumTest Potassium { get { return SerumTests[SerumTestTypes.Potassium].LastOrDefault(); } }
        public SerumTest TCO2 { get { return SerumTests[SerumTestTypes.TCO2].LastOrDefault(); } }
        public SerumTest Chloride { get { return SerumTests[SerumTestTypes.Chloride].LastOrDefault(); } }
        public SerumTest BUN { get { return SerumTests[SerumTestTypes.BUN].LastOrDefault(); } }
        public SerumTest Creatinine { get { return SerumTests[SerumTestTypes.Creatinine].LastOrDefault(); } }
        public SerumTest Glucose { get { return SerumTests[SerumTestTypes.Glucose].LastOrDefault(); } }
        public SerumTest PT { get { return SerumTests[SerumTestTypes.PT].LastOrDefault(); } }
        public SerumTest PTT { get { return SerumTests[SerumTestTypes.PTT].LastOrDefault(); } }
        public SerumTest INR { get { return SerumTests[SerumTestTypes.INR].LastOrDefault(); } }
        public SerumTest Albumin { get { return SerumTests[SerumTestTypes.Albumin].LastOrDefault(); } }
        public SerumTest AST_ALT { get { return SerumTests[SerumTestTypes.AST_ALT].LastOrDefault(); } }
        public SerumTest Troponin { get { return SerumTests[SerumTestTypes.Troponin].LastOrDefault(); } }
        public SerumTest BNP { get { return SerumTests[SerumTestTypes.BNP].LastOrDefault(); } }
        public SerumTest CK { get { return SerumTests[SerumTestTypes.CK].LastOrDefault(); } }
        public SerumTest CKMB { get { return SerumTests[SerumTestTypes.CKMB].LastOrDefault(); } }

        /////////////////////////////////////////////////////////////////////
        #endregion
        /////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////
        #region Images
        /////////////////////////////////////////////////////////////////////

        public List<ImageStudy> ImageStudies { get; set; } = new List<ImageStudy>();

        /////////////////////////////////////////////////////////////////////
        #endregion
        /////////////////////////////////////////////////////////////////////

    }

    /////////////////////////////////////////////////////////////////////
    #region Enumerations
    /////////////////////////////////////////////////////////////////////

    public enum Genders
    {
        Male,
        Female,
        Other,
        Unknown
    }

    public enum HeightUnits
    {
        Inches,
        Centimeters
    }

    public enum WeightUnits
    {
        Pounds,
        Kilograms
    }

    public enum SerumTestTypes
    {
        Hematocrit,         // 4544-3
        Hemoglobin,         // 718-7       
        PlateletCount,      // 777-3
        Sodium,             // 2951-2
        Potassium,          // 2823-3
        TCO2,               // 2028-9
        Chloride,           // 2075-0
        BUN,                // 3094-0
        Creatinine,         // 2160-0
        Glucose,            // 2345-7
        PT,                 // 5902-2
        PTT,                // 3173-2
        INR,                // 6301-6
        Albumin,            // 1751-7
        AST_ALT,            // 1916-6
        Troponin,           // 10839-9
        BNP,                // 30934-4
        CK,                 // 2157-6
        CKMB                // 32673-6
    }

    /////////////////////////////////////////////////////////////////////
    #endregion
    /////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////
    #region Helper Classes
    /////////////////////////////////////////////////////////////////////

    public class SerumTest
    {
        public double Value { get; set; }
        public string Unit { get; set; }
        public DateTimeOffset EffectiveDate { get; set; }
    }

    /////////////////////////////////////////////////////////////////////
    #endregion
    /////////////////////////////////////////////////////////////////////


}
