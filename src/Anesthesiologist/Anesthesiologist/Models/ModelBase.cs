﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anesthesiologist.Models
{
    public abstract class ModelBase
    {
        public override string ToString()
        {
            var f = new JsonFormatter(JsonConvert.SerializeObject(this));
            return f.Format();
        }
    }
}
