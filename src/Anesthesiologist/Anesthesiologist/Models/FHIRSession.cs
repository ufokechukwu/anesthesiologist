﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anesthesiologist.Models
{
    public class FHIRSession
    {
        public string ProviderConfigID { get; set; }

        public List<Patient> Patients { get; set; } = new List<Patient>();

        //private DateTime dateCreated { get; set; }
        //public DateTime DateCreated { get { return dateCreated; } }

        public FHIRSession()
        {
        }

        public FHIRSession(List<Patient> Patients, string ProviderConfigID)
        {
            //dateCreated = DateTime.Now;
            this.Patients = new List<Patient>();
            this.Patients = Patients;
            this.ProviderConfigID = ProviderConfigID;
        }
    }

    public class AllFHIRSessions
    {
        public List<FHIRSession> Sessions { get; set; } = new List<FHIRSession>();
        //public DateTime LastModified { get; set; }
        //private DateTime dateCreated { get; set; }
        //public DateTime DateCreated { get { return dateCreated; } }

        public AllFHIRSessions()
        {
        }

        public AllFHIRSessions(List<FHIRSession> Sessions)
        {
            //dateCreated = DateTime.Now;
            this.Sessions = new List<FHIRSession>();
            this.Sessions = Sessions;
        }
    }

}
