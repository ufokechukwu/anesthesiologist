﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anesthesiologist.Models
{
    public class ImageStudy
    {
        public Uri BaseUri { get; set; }
        public string Description { get; set; }
        public DateTimeOffset? DateStarted { get; set; }
        public List<ImageStudySeries> Series { get; set; } = new List<ImageStudySeries>();
    }

    public class ImageStudySeries
    {
        public int Number { get; set; }
        public string Description { get; set; }
        public List<ImageStudyImage> Images { get; set; } = new List<ImageStudyImage>();
    }

    public class ImageStudyImage
    {
        public int Number { get; set; }
        public string Title { get; set; }
        public Uri FullUri { get; set; }
        public string ContentType { get; set; }
    }
}
