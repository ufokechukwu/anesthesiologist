﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anesthesiologist.Models
{
    public static class DateExtensions
    {
        public static string ToISO8601String(this DateTime date)
        {
            return date.ToString("yyyy-MM-ddThh:mm:sszzz");
        }

        public static string ToISO8601String(this DateTimeOffset date)
        {
            return date.ToString("yyyy-MM-ddThh:mm:sszzz");
        }
    }
}
