﻿using Anesthesiologist.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using System.Threading.Tasks;
using Anesthesiologist.DAL.FHIR;
using System.Diagnostics;

namespace Anesthesiologist
{
	public partial class ServerInfoPage : ContentPage
	{
        IDataProviderConfig _config = null;

        public ServerInfoPage(IDataProviderConfig config)
		{
            InitializeComponent();

            Title = "Server: " + config.Name;

            _config = config;
            this.BindingContext = _config;

            deleteButton.Clicked += async (s, e) =>
            {
                if (await DisplayAlert("Confirm Delete", "Are you sure you wish to delete this server?", "Yes", "No"))
                {
                    await DALFactory.RemoveConfigurationAsync(BindingContext as IDataProviderConfig);
                    await Navigation.PopAsync();
                }
            };

            activateButton.Clicked += async (s, e) =>
            {
                DALFactory.SetActiveConfiguration(config);
                await Navigation.PopAsync();
            };


            activateButton.IsEnabled = _config.IsActiveConfig ? false : true;
            activateButton.BackgroundColor = _config.IsActiveConfig ? Color.Gray : Color.Green;
            activateButton.Text = _config.IsActiveConfig ? "ACTIVE" : "SET ACTIVE";

            if (_config is FHIRDataProviderConfig)
            {
                var server = BindingContext as DAL.FHIR.FHIRDataProviderConfig;
                AddGridRow("Endpoint:", server.Endpoint?.AbsoluteUri);
                AddGridRow("Authentication:", server.AuthType.ToString());

                if (server.AuthType == FHIRAuthType.SMART && server.SmartAuth != null)
                {
                    AddGridRow("App Name:", server.SmartAuth.AppName);
                    AddGridRow("Client ID:", server.SmartAuth.ClientId);
                    AddGridRow("Scope:", server.SmartAuth.Scope);
                    AddGridRow("Base URI:", server.SmartAuth.SmartUris.BaseUri);
                    AddGridRow("Authorize URI:", server.SmartAuth.SmartUris.AuthorizeUri);
                    AddGridRow("Redirect URI:", server.SmartAuth.RedirectUri);
                    AddGridRow("Logo URI:", server.SmartAuth.LogoUri);
                    AddGridRow("Register URI:", server.SmartAuth.SmartUris.RegisterUri);
                    AddGridRow("Token URI:", server.SmartAuth.SmartUris.TokenUri);
                    AddGridRow("Has Token:", server.SmartAuth.HasToken.ToString());
                    if (server.SmartAuth.HasToken)
                    {
                        AddGridRow("Token Expiration:", server.SmartAuth.TokenExpiration.ToString());
                    }
                    AddGridRow("Has Refresh Token:", server.SmartAuth.HasRefreshToken.ToString());
                }
            }
        }

        private void AddGridRow(string header, object text)
        {
            serverInfoGrid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            var rownum = serverInfoGrid.RowDefinitions.Count - 1;
            serverInfoGrid.Children.Add(new Label()
            {
                Text = header,
                TextColor = Color.Blue,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                FontAttributes = FontAttributes.Bold
            }, 0, rownum);
            serverInfoGrid.Children.Add(new Label()
            {
                Text = text != null ? text.ToString() : "",
                TextColor = Color.Blue,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label))
            }, 1, rownum);
        }
    }
}
