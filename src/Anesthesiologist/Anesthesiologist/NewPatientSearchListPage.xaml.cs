﻿using Anesthesiologist.DAL;
using Anesthesiologist.DAL.FHIR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Anesthesiologist
{
    public partial class NewPatientSearchListPage : ContentPage
    {
        List<Models.Patient> itemsDataSource;

        public NewPatientSearchListPage(DAL.FHIR.FHIRSearchType searchType, string input)
        {
            InitializeComponent();
            Title = "SEARCH (" + input + ")";

            addButton.Clicked += AddButton_Clicked;
            cancelButton.Clicked += CancelButton_Clicked;

            SearchForPatientsWith(searchType, input);
        }

        private async void CancelButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private async void AddButton_Clicked(object sender, EventArgs e)
        {
            if (PatientsListXAML.SelectedItem != null)
            {
                var patient = PatientsListXAML.SelectedItem as Models.Patient;

                var config = await DALFactory.GetConfigurationAsync(patient.ProviderConfigId);
                if (config != null)
                {
                    using (var dal = await DALFactory.GetProviderAsync(config))
                    {
                        await dal.RefreshPatientAsync(patient);
                    }
                    await IDataStorage.SavePatient(patient, config.UUID);
                    await Navigation.PopToRootAsync();
                }
                else
                {
                    await DisplayAlert("Alert!", "ERROR SAVING PATIENT", "OK");
                }
            }
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            addButton.IsEnabled = true;
        }



        private async void SearchForPatientsWith(FHIRSearchType searchType, string input)
        {
            itemsDataSource = new List<Models.Patient>();

            // Do Patient Search Here
            var config = await DALFactory.GetActiveConfigurationAsync();
            if (config != null)
            {
                itemsDataSource = await SearchForPatients(searchType, input, config);

                PatientsListXAML.ItemsSource = itemsDataSource;
                // Get and load results of searched patients.
                PatientsListXAML.ItemSelected += OnItemSelected;
            }
        }

        private async Task<List<Models.Patient>> SearchForPatients(FHIRSearchType searchType, string input, IDataProviderConfig config)
        {
            var patientsList = new List<Models.Patient>();
            try
            {
                switch (searchType)
                {
                    case FHIRSearchType.ID:

                        using (var dal = await DALFactory.GetProviderAsync(config, this.Navigation))
                        {
                            var patient = await dal.GetPatientByMRNAsync(input);
                            patientsList.Add(patient);
                        }

                        return patientsList;

                    case FHIRSearchType.LastName:
                    case FHIRSearchType.GivenName:
                    case FHIRSearchType.Name:
                        // Find Patient By Last Name
                        using (var dal = await DALFactory.GetProviderAsync(config, this.Navigation))
                        {
                            var patient = await dal.GetPatientByNameAsync(input, searchType);
                            patientsList = patient.ToList();
                        }
                        return patientsList;

                    default:
                        return patientsList;

                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error!", "An Error occured : " + ex.Message, "OK");
            }

            return patientsList;
        }

        //private async Task<List<Models.Patient>> SearchForPatients_SMART(FHIRSearchType searchType, string input, IDataProviderConfig config)
        //{
        //    //  NO OAUTH REQUIRED
        //    switch (searchType)
        //    {
        //        case FHIRSearchType.ID:
        //            //Find Patients By ID
        //            return null;

        //        case FHIRSearchType.LastName:
        //            // Find Patient By Last Name
        //            return null;

        //        case FHIRSearchType.GivenName:
        //            // Find Patient By Part of Given Name
        //            return null;

        //        case FHIRSearchType.Name:
        //            // Find Patient By Name
        //            return null;

        //        default:
        //            return null;
        //    }
        //}

        //private async Task<List<Models.Patient>> SearchForPatients_BASIC(FHIRSearchType searchType, string input, IDataProviderConfig config)
        //{
        //    //  NO OAUTH REQUIRED
        //    switch (searchType)
        //    {
        //        case FHIRSearchType.ID:
        //            //Find Patients By ID
        //            return null;

        //        case FHIRSearchType.LastName:
        //            // Find Patient By Last Name
        //            return null;

        //        case FHIRSearchType.GivenName:
        //            // Find Patient By Part of Given Name
        //            return null;

        //        case FHIRSearchType.Name:
        //            // Find Patient By Name
        //            return null;

        //        default:
        //            return null;
        //    }
        //}

        //async Task<List<Models.Patient>> FindPatientByID_No_OAuth(string ID, IDataProviderConfig config)
        //{
        //    var patientsList = new List<Models.Patient>();
        //    try
        //    {
        //        using (var dal = await DALFactory.GetProviderAsync(config, this.Navigation))
        //        {
        //            var patient = await dal.GetPatientByMRNAsync(ID);
        //            patientsList.Add(patient);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        await DisplayAlert("Error!", "An Error occured : " + ex.Message, "OK");
        //    }

        //    return patientsList;

        //}
    }
}
