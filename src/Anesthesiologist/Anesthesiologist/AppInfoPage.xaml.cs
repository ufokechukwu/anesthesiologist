﻿using Akavache;
using Anesthesiologist.DAL;
using Anesthesiologist.DAL.FHIR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace Anesthesiologist
{
    public partial class AppInfoPage : ContentPage
    {
        private StackLayout _layout;

        public AppInfoPage()
        {
            InitializeComponent();

            Title = "Info";

            _layout = new StackLayout();

            Button b1 = new Button()
            {
                Text = "Run a test..."
            };
            b1.Clicked += b1_Clicked;

            this.Content = new StackLayout
            {
                Children =
                {
                    b1,
                    new ScrollView
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        Content = _layout
                    }
                }
            };

        }

        private async void b1_Clicked(object sender, EventArgs e)
        {
            var tests = new string[]
            {
                "Get Sample (MOCK)",
                "Get Marla (FHIR GATECH)",
                "Test Login (FHIR SMART)",
                "Get Allen Vitalis (FHIR SMART)",
                "Get Amy Shaw (RUNNING ON FHIR)",
                "List Provider Configurations (Servers)",
                "Clear Provider Configurations (Servers)"
            };
            var test = await DisplayActionSheet("Which Test?", "Cancel", null, tests);

            if (test == tests[0])
            {
				try
				{
					await Test1();	
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}

            }
            else if (test == tests[1])
            {
				try
				{
					await Test2();
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}
            }
            else if (test == tests[2])
            {
                await Test3();
            }
            else if (test == tests[3])
            {
                await Test4();
            }
            else if (test == tests[4])
            {
                await Test4b();
            }
            else if (test == tests[5])
            {
                await Test5();
            }
            else if (test == tests[6])
            {
                await Test6();
            }
        }

        public async Task Test1()
        {
            var mock = new DAL.Mock.MockDataProviderConfig();
            using (var dal = await DALFactory.GetProviderAsync(mock, this.Navigation))
            {
                var patient = await dal.GetPatientByProviderIdAsync("12345");

                _layout.Children.Clear();
                _layout.Children.Add(new Label()
                {
                    Text = patient.ToString()
                });
            }
        }

        public async Task Test2()
        {
            _layout.Children.Clear();

            var name = "Tech on FHIR";

            // Just need to get a configuration from the factory
            var providers = await DALFactory.GetAllConfigurationsAsync();
            providers = await DALFactory.GetAllConfigurationsAsync<FHIRDataProviderConfig>();
            IDataProviderConfig config = providers.FirstOrDefault(p => p.Name == name);

            // THIS PART IS BOGUS TEMP CODE THAT SHOULD GO IN A NEW SERVER GUI
            if (config == null)
            {
                config = new FHIRDataProviderConfig()
                {
                    Endpoint = new Uri("http://polaris.i3l.gatech.edu:8080/fhir-omopv5/base"),
                    Name = name
                };

                await DALFactory.AddConfigurationAsync(config);
            }
            // END BOGUS TEMP CODE

            // And now create + use the provider itself
            try
            {
                using (var dal = await DALFactory.GetProviderAsync(config, this.Navigation))
                {
                    //var patient = await dal.GetPatientByProviderIdAsync("523050");
                    var patient = await dal.GetPatientByMRNAsync("523050");
                    //var patient = await dal.GetPatientByFINAsync("28223050");
                    await dal.RefreshPatientAsync(patient);

                    _layout.Children.Add(new Label()
                    {
                        Text = patient.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                _layout.Children.Add(new Label()
                {
                    Text = "Error Retrieving Patient: " + ex.Message
                });
            }
        }

        public async Task Test3()
        {
            var smart = new SmartAuth(new SmartUris("https://fhir-api-dstu2.smarthealthit.org"), "Anesthesiologist App", "user/*.*");
            try
            {
                await smart.InitializeAsync(Navigation);
                if (smart.HasValidToken)
                {
                    _layout.Children.Add(new Label()
                    {
                        Text = $"SMART Initialized \nToken: {smart.Token} (exp: {smart.TokenExpiration})"
                    });
                }
                else
                {
                    _layout.Children.Add(new Label()
                    {
                        Text = "Error Initializing SMART: Unable to acquire token"
                    });
                }
            }
            catch (Exception ex)
            {
                _layout.Children.Add(new Label()
                {
                    Text = "Error Initializing SMART: " + ex.Message
                });
            }
        }


        public async Task Test4()
        {
            _layout.Children.Clear();

            var name = "SMART on FHIR Public Sandbox";

            // Just need to get a configuration from the factory
            var providers = await DALFactory.GetAllConfigurationsAsync<FHIRDataProviderConfig>();
            var config = providers.FirstOrDefault(p => p.Name == name);

            // THIS PART IS BOGUS TEMP CODE THAT SHOULD GO IN A NEW SERVER GUI
            if (config == null)
            {
                var baseUrl = "https://fhir-api-dstu2.smarthealthit.org";
                config = new FHIRDataProviderConfig()
                {
                    Endpoint = new Uri(baseUrl),
                    Name = name
                };
                await config.FetchConformanceAsync();
                var auth = new SmartAuth(config.SmartUris, "Anesthesiologist App", "user/*.read offline_access");
                config.SetAuth(auth);

                await DALFactory.AddConfigurationAsync(config);
            }
            // END BOGUS TEMP CODE

            // And now create + use the provider itself
            try
            {
                using (var dal = await DALFactory.GetProviderAsync(config, this.Navigation))
                {
                    // Get Allen Vitalis as he's got lots of documents, images, etc.
                    var patient = await dal.GetPatientByProviderIdAsync("99912345");
                    await dal.RefreshPatientAsync(patient);

                    _layout.Children.Add(new Label()
                    {
                        Text = patient.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                _layout.Children.Add(new Label()
                {
                    Text = "Error Retrieving Patient: " + ex.Message
                });
            }
        }

        public async Task Test4b()
        {
            _layout.Children.Clear();

            var name = "RUNNING ON FHIR Sandbox";

            // Just need to get a configuration from the factory
            var providers = await DALFactory.GetAllConfigurationsAsync<FHIRDataProviderConfig>();
            var config = providers.FirstOrDefault(p => p.Name == name);

            // THIS PART IS BOGUS TEMP CODE THAT SHOULD GO IN A NEW SERVER GUI
            if (config == null)
            {
                var baseUrl = "http://runningonfhir.com:9080/data";
                config = new FHIRDataProviderConfig()
                {
                    Endpoint = new Uri(baseUrl),
                    Name = name
                };
                await config.FetchConformanceAsync();
                var auth = new SmartAuth(config.SmartUris, "Anesthesiologist App", "user/*.read offline_access");
                config.SetAuth(auth);

                await DALFactory.AddConfigurationAsync(config);
            }
            // END BOGUS TEMP CODE

            // And now create + use the provider itself
            try
            {
                using (var dal = await DALFactory.GetProviderAsync(config, this.Navigation))
                {
                    // Get Amy Shaw as she's got lots of data
                    var patient = await dal.GetPatientByMRNAsync("1540505");
                    await dal.RefreshPatientAsync(patient);

                    _layout.Children.Add(new Label()
                    {
                        Text = patient.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                _layout.Children.Add(new Label()
                {
                    Text = "Error Retrieving Patient: " + ex.Message
                });
            }
        }

        public async Task Test5()
        {
            _layout.Children.Clear();
            var configurations = await DALFactory.GetAllConfigurationsAsync();
            foreach (var config in configurations)
            {
                _layout.Children.Add(new Label()
                {
                    Text = $"{config.Name} ({config.GetType().Name}: {config.Id})"
                });

            }
        }

        public async Task Test6()
        {
            _layout.Children.Clear();
            await DALFactory.RemoveAllConfigurationsAsync();
            _layout.Children.Add(new Label()
            {
                Text = $"All Configurations Cleared!"
            });
        }
    }
}
