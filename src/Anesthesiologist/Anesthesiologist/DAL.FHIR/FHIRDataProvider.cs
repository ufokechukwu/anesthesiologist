﻿using Anesthesiologist.Models;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Anesthesiologist.DAL.FHIR
{
    public enum FHIRSearchType
    {
        ID,
        LastName,
        GivenName,
        Name
    }

    public class FHIRDataProvider : IDataProvider
    {
        FHIRDataProviderConfig _config;
        FhirClient _client;

        public bool IsInitialized
        {
            get
            {
                if (_client == null || _config == null)
                {
                    return false;
                }
                if (_config.AuthType == FHIRAuthType.Basic)
                {
                    return !string.IsNullOrWhiteSpace(_config.BasicAuth);
                }
                if (_config.AuthType == FHIRAuthType.SMART)
                {
                    return (bool)_config.SmartAuth?.HasValidToken;
                }
                return true;
            }
        }

        public async Task InitializeAsync(IDataProviderConfig config, INavigation navigation = null)
        {
            _config = config as FHIRDataProviderConfig;
            if (_config == null)
            {
                throw new ArgumentException("options null or incorrect data type");
            }

            if (!_config.HasConformance)
            {
                await _config.FetchConformanceAsync();
            }

            _client = new FhirClient(_config.Endpoint);
            _client.OnBeforeRequest += _client_OnBeforeRequest;

            if (_config.AuthType == FHIRAuthType.SMART)
            {
                if (!_config.SmartAuth.HasValidToken)
                {
                    await _config.SmartAuth.InitializeAsync(navigation);
                    if (!_config.SmartAuth.HasValidToken)
                    {
                        throw new InvalidOperationException("Unable to initialize SMART Authorization using specified parameters");
                    }
                }
            }
        }

        private void _client_OnBeforeRequest(object sender, BeforeRequestEventArgs e)
        {
            try
            {
                if (_config.AuthType == FHIRAuthType.SMART)
                {
                    e.RawRequest.Headers["Authorization"] = $"Bearer {_config.SmartAuth.Token}";
                }
                else if (_config.AuthType == FHIRAuthType.Basic)
                {
                    e.RawRequest.Headers["Authorization"] = $"Basic {_config.BasicAuth}";
                }
            }
            catch { } // eat it
        }

        public async Task<IEnumerable<Models.Patient>> FindPatientsAsync(string familyName, string givenName)
        {
            return await Task.Run<IEnumerable<Models.Patient>>(() =>
            {
                var parms = new SearchParams();

                if (!string.IsNullOrWhiteSpace(familyName))
                    parms.Add("family", familyName);

                if (!string.IsNullOrWhiteSpace(givenName))
                    parms.Add("given", givenName);

                if (parms.Count == 0)
                    throw new InvalidOperationException("Must supply one or more values");

                var results = _client.Search<Hl7.Fhir.Model.Patient>(parms);
                if (results != null && results.Entry.Any())
                {
                    var patients = new List<Models.Patient>();
                    foreach (var result in results.Entry)
                    {
                        var res = result.Resource as Hl7.Fhir.Model.Patient;
                        patients.Add(_ReadPatient(res));
                    }
                    return patients;
                }

                return null;
            });
        }

        public async Task<Models.Patient> GetPatientByFINAsync(string fin)
        {
            return await Task.Run<Models.Patient>(async () =>
            {
                var results = _client.Search<Hl7.Fhir.Model.Encounter>(new string[] { $"_id:exact={fin}" });
                if (results != null && results.Entry.Any() && results.Entry.FirstOrDefault().HasResource())
                {
                    var res = results.Entry.FirstOrDefault().Resource as Hl7.Fhir.Model.Encounter;
                    var patref = res.Patient.Reference;
                    if (!string.IsNullOrWhiteSpace(patref))
                    {
                        var patid = patref.Split('/').Last();
                        return await GetPatientByProviderIdAsync(patid);
                    }
                }

                return null;
            });
        }

        public async Task<Models.Patient> GetPatientByMRNAsync(string mrn)
        {
            return await Task.Run<Models.Patient>(() =>
            {
                //var results = _client.Search<Hl7.Fhir.Model.Patient>(new string[] { $"identifier={mrn}" }); // STUPID Tech FHIR Server...FHIR is REALLY annoying
                var results = _client.Search<Hl7.Fhir.Model.Patient>(new string[] { $"_id={mrn}" });
                if (results != null && results.Entry.Any() && results.Entry.FirstOrDefault().HasResource())
                {
                    var res = results.Entry.FirstOrDefault().Resource as Hl7.Fhir.Model.Patient;
                    var patient = _ReadPatient(res);
                    return patient;
                }

                return null;
            });
        }

        public async Task<Models.Patient> GetPatientByProviderIdAsync(string id)
        {
            return await Task.Run<Models.Patient>(() =>
            {
                var results = _client.SearchById<Hl7.Fhir.Model.Patient>(id);
                if (results != null && results.Entry.Any() && results.Entry.FirstOrDefault().HasResource())
                {
                    var res = results.Entry.FirstOrDefault().Resource as Hl7.Fhir.Model.Patient;
                    var patient = _ReadPatient(res);
                    return patient;
                }

                return null;
            });
        }

        // TODO: Duplicate of FindPatientsAsync()
        public async Task<IEnumerable<Models.Patient>> GetPatientByNameAsync(string name, FHIRSearchType searchCriteria)
        {
            return await Task.Run<IEnumerable<Models.Patient>>(() =>
            {
                Bundle results = new Bundle();
                if (searchCriteria == FHIRSearchType.LastName)
                {
                    results = _client.Search<Hl7.Fhir.Model.Patient>(new string[] { $"family={name}" });
                }
                else if (searchCriteria == FHIRSearchType.GivenName)
                {
                    results = _client.Search<Hl7.Fhir.Model.Patient>(new string[] { $"given={name}" });
                }
                else
                {
                    results = _client.Search<Hl7.Fhir.Model.Patient>(new string[] { $"name={name}" });
                }

                List<Models.Patient> patients = new List<Models.Patient>();

                if (results != null && results.Entry.Any())
                {
                    foreach (var item in results.Entry)
                    {
                        var res = item.Resource as Hl7.Fhir.Model.Patient;
                        var patient_data = _ReadPatient(res);
                        patients.Add(patient_data);
                    }
                }

                return patients;
            });
        }

        public async Task<bool> RefreshPatientAsync(Models.Patient patient, bool force = false)
        {
            return await RefreshPatientsAsync(new List<Models.Patient> { patient }, force);
        }

        public async Task<bool> RefreshPatientsAsync(IEnumerable<Models.Patient> patients, bool force = false)
        {
            return await Task.Run<bool>(() =>
            {
                bool wasModified = false;
                foreach (var patient in patients)
                {
                    if (force || patient.ShouldRefresh)
                    {
                        DateTimeOffset lastUpdated = DateTimeOffset.MinValue;

                        ////////////////////////////////////////////////////////////
                        // LOAD OBSERVATIONS
                        ////////////////////////////////////////////////////////////

                        List<string> loincs = new List<string>();

                        loincs.AddRange(Models.Patient.HeightLoincCodes);
                        loincs.AddRange(Models.Patient.WeightLoincCodes);
                        foreach (var test in Models.Patient.SerumLoincMap.Keys)
                        {
                            loincs.AddRange(Models.Patient.SerumLoincMap[test]);
                        }

                        SearchParams parms = null;

                        // TODO: Implement quick refresh. Need to check metadata (conformance) for support for _lastUpdate
                        //       Explore Type History
                        if (true)//patient.LastRefreshed == DateTime.MinValue)
                        {
                            // do full refresh
                            foreach (var list in patient.SerumTests.Values)
                            {
                                list.Clear();
                            }
                            parms = new SearchParams()
                                        .Add("patient", patient.ProviderID)
                                        .Add("code", string.Join(",", loincs));
                        }
                        else
                        {
                            parms = new SearchParams()
                                        .Add("patient", patient.ProviderID)
                                        .Add("code", string.Join(",", loincs))
                                        .Add("_lastUpdated", patient.LastUpdated.ToISO8601String());
                        }

                        var heightLatestDate = DateTime.MinValue;
                        var weightLatestDate = DateTime.MinValue;
                        var results = _client.Search<Observation>(parms);
                        while (results != null)
                        {
                            if (results.Meta != null && results.Meta.LastUpdated.HasValue && results.Meta.LastUpdated.Value > lastUpdated)
                            {
                                lastUpdated = results.Meta.LastUpdated.Value;
                            }

                            foreach (var entry in results.Entry)
                            {
                                wasModified = true;

                                var obs = entry.Resource as Observation;
                                var code = obs.Code?.Coding?.FirstOrDefault()?.Code;
                                if (code == null) continue;
                                var qty = obs.Value as Quantity;
                                if (qty == null || !qty.Value.HasValue) continue;

                                if (obs.Meta != null && obs.Meta.LastUpdated.HasValue && obs.Meta.LastUpdated.Value > lastUpdated)
                                {
                                    lastUpdated = obs.Meta.LastUpdated.Value;
                                }

                                // HEIGHT
                                if (Models.Patient.HeightLoincCodes.Contains(code))
                                {
                                    DateTime date = DateTime.MinValue;
                                    if (DateTime.TryParse(obs.Effective.ToString(), out date) && date > heightLatestDate)
                                    {
                                        heightLatestDate = date;
                                        patient.Height = (double)qty.Value;
                                        patient.HeightUnit = qty.Unit?.ToLower() == "cm" ? HeightUnits.Centimeters : HeightUnits.Inches;
                                    }
                                }

                                // WEIGHT
                                if (Models.Patient.WeightLoincCodes.Contains(code))
                                {
                                    DateTime date = DateTime.MinValue;
                                    if (DateTime.TryParse(obs.Effective.ToString(), out date) && date > weightLatestDate)
                                    {
                                        weightLatestDate = date;
                                        patient.Weight = (double)qty.Value;
                                        patient.WeightUnit = qty.Unit?.ToLower() == "kg" ? WeightUnits.Kilograms : WeightUnits.Pounds;
                                    }
                                }

                                // OTHER OBSERVATIONS
                                foreach (var test in Models.Patient.SerumLoincMap.Keys)
                                {
                                    if (Models.Patient.SerumLoincMap[test].Contains(code))
                                    {
                                        var testObj = new SerumTest
                                        {
                                            Value = (double)qty.Value.Value,
                                            Unit = qty.Unit,
                                            EffectiveDate = DateTime.MinValue
                                        };

                                        var effective = obs.Effective as FhirDateTime;
                                        if (effective != null)
                                        {
                                            testObj.EffectiveDate = effective.ToDateTimeOffset();
                                        }

                                        patient.SerumTests[test].Add(testObj);
                                        break;
                                    }
                                }
                            }
                            results = _client.Continue(results);
                        }

                        ////////////////////////////////////////////////////////////
                        // LOAD IMAGE STUDIES
                        ////////////////////////////////////////////////////////////

                        // TODO: Implement quick refresh. Need to check metadata (conformance) for support for _lastUpdate
                        //       Explore Type History
                        if (true)//patient.LastRefreshed == DateTime.MinValue)
                        {
                            // do full refresh
                            patient.ImageStudies.Clear();
                            parms = new SearchParams()
                                        .Add("patient", patient.ProviderID);
                        }
                        else
                        {
                            parms = new SearchParams()
                                        .Add("patient", patient.ProviderID)
                                        .Add("_lastUpdated", patient.LastUpdated.ToISO8601String());
                        }

                        // Might not have ImageStudies so separate try/catch here
                        // TODO: Check metadata to see if ImageStudies are available
                        try
                        {
                            results = _client.Search<ImagingStudy>(parms);
                        }
                        catch
                        {
                            results = null;
                        }
                        while (results != null)
                        {
                            if (results.Meta != null && results.Meta.LastUpdated.HasValue && results.Meta.LastUpdated.Value > lastUpdated)
                            {
                                lastUpdated = results.Meta.LastUpdated.Value;
                            }

                            foreach (var entry in results.Entry)
                            {
                                wasModified = true;

                                Uri uri;
                                if (!Uri.TryCreate(entry.FullUrl, UriKind.Absolute, out uri)) continue;

                                var fhirStudy = entry.Resource as ImagingStudy;
                                if (fhirStudy.Series == null || fhirStudy.Series.Count == 0) continue;

                                if (fhirStudy.Meta != null && fhirStudy.Meta.LastUpdated.HasValue && fhirStudy.Meta.LastUpdated.Value > lastUpdated)
                                {
                                    lastUpdated = fhirStudy.Meta.LastUpdated.Value;
                                }

                                var study = new ImageStudy();

                                study.BaseUri = uri;
                                DateTimeOffset started;
                                if (DateTimeOffset.TryParse(fhirStudy.Started, out started))
                                {
                                    study.DateStarted = started;
                                }
                                study.Description = fhirStudy.Description;

                                foreach (var fhirSeries in fhirStudy.Series)
                                {
                                    if (fhirSeries.Instance == null || fhirSeries.Instance.Count == 0) continue;

                                    var series = new ImageStudySeries();
                                    series.Description = fhirSeries.Description;
                                    series.Number = fhirSeries.Number.GetValueOrDefault();
                                    
                                    foreach(var fhirInstance in fhirSeries.Instance)
                                    {
                                        var content = fhirInstance.Content.FirstOrDefault();
                                        if (content == null) continue;

                                        var image = new ImageStudyImage();
                                        image.ContentType = content.ContentType;
                                        Uri imageUri;
                                        if (Uri.TryCreate(study.BaseUri, content.Url, out imageUri) ||
                                            Uri.TryCreate(content.Url, UriKind.Absolute, out imageUri))
                                        {
                                            image.FullUri = imageUri;
                                        }
                                        else
                                        {
                                            continue;
                                        }
                                        image.Number = fhirInstance.Number.GetValueOrDefault();
                                        image.Title = fhirInstance.Title;

                                        series.Images.Add(image);
                                    }

                                    if (series.Images.Count > 0)
                                    {
                                        study.Series.Add(series);
                                    }
                                }

                                if (study.Series.Count > 0)
                                {
                                    patient.ImageStudies.Add(study);
                                }
                            }
                            results = _client.Continue(results);
                        }

                        if (wasModified)
                        {
                            foreach (var list in patient.SerumTests.Values)
                            {
                                list.Sort((x,y) => x.EffectiveDate.CompareTo(y.EffectiveDate));
                            }
                        }

                        patient.LastUpdated = lastUpdated;
                        patient.LastRefreshed = DateTime.Now;
                    }
                }
                return wasModified;
            });
        }

        private Models.Patient _ReadPatient(Hl7.Fhir.Model.Patient fhirPatient)
        {
            if (fhirPatient == null) return null;

            var patient = new Models.Patient();

            patient.ProviderConfigId = _config.Id;

            patient.ProviderID = fhirPatient.Id;
            patient.MRN = fhirPatient.Id;

            var name = fhirPatient.Name.FirstOrDefault();
            if (name != null)
            {
                patient.NamesGiven.AddRange(name.Given);
                patient.NameFamily = String.Join(" ", name.Family);
                patient.NamePrefix = String.Join(" ", name.Prefix);
                patient.NameSuffix = String.Join(" ", name.Suffix);
            }

            if (fhirPatient.Gender == AdministrativeGender.Female) patient.Gender = Genders.Female;
            else if (fhirPatient.Gender == AdministrativeGender.Male) patient.Gender = Genders.Male;
            else if (fhirPatient.Gender == AdministrativeGender.Other) patient.Gender = Genders.Other;

            DateTime tmpDate;
            if (DateTime.TryParse(fhirPatient.BirthDate, out tmpDate))
            {
                patient.BirthDate = tmpDate;
            }

            var address = fhirPatient.Address.FirstOrDefault(a => a.Use == Address.AddressUse.Home);
            if (address != null)
            {
                patient.Location = address.City + ", " + address.State;
            }

            return patient;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // xTODO: dispose managed state (managed objects).
                }

                // xTODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // xTODO: set large fields to null.

                disposedValue = true;
            }
        }

        // xTODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DataProvider() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // xTODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
