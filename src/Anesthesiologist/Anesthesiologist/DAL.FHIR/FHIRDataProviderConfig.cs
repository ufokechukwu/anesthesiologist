﻿using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Anesthesiologist.DAL.FHIR
{
    public class FHIRDataProviderConfig : IDataProviderConfig
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; }

        public string Description
        {
            get
            {
                return "FHIR" + _endpoint != null ? $" – {_endpoint.Host}" : "";
            }
        }

        public string UUID { get { return Id + Name; } }

        public bool IsActiveConfig { get; set; }

        private Uri _endpoint;

        public Uri Endpoint
        {
            get { return _endpoint; }
            set
            {
                _endpoint = value;
                SmartUris = new SmartUris(value);
            }
        }

        public FHIRAuthType AuthType { get; set; } = FHIRAuthType.None;

        // EITHER USE SMART ON FHIR OAUTH
        private SmartAuth _smartAuth;

        public SmartAuth SmartAuth
        {
            get { return _smartAuth; }
            set
            {
                _smartAuth = value;
                if (_smartAuth != null)
                {
                    _smartAuth.ShouldSave += (s, e) => { ShouldSave?.Invoke(s, e); };
                }
            }
        }

        // OR USE USERID + PASSWORD
        public string BasicAuth { get; set; }

        public XDocument ConformanceXml { get; set; }

        public JObject ConformanceJson { get; set; }

        public Uri ConformanceEndpoint { get; set; }

        public bool HasConformance { get { return Endpoint == ConformanceEndpoint && (ConformanceXml != null || ConformanceJson != null); } }

        public string ConformancePublisher { get; set; }

        public string ConformanceSecurityService { get; set; }

        public SmartUris SmartUris { get; set; }

        public bool SupportsAutoRegistration { get { return SmartUris?.RegisterUri != null; } }


        public event EventHandler ShouldSave;

        public IDataProvider CreateInstance()
        {
            if (this.Endpoint == null)
            {
                throw new InvalidOperationException("Must set endpoint prior to creating an instance.");
            }
            var provider = new FHIRDataProvider();
            return provider;
        }

        public void SaveChanges()
        {
            ShouldSave?.Invoke(this, new EventArgs());
        }

        public void SetAuth(SmartAuth smart)
        {
            this.SmartAuth = smart;
            this.BasicAuth = null;
            this.AuthType = FHIRAuthType.SMART;
            ShouldSave?.Invoke(this, new EventArgs());
        }

        public void SetAuth(string uid, string pwd)
        {
            this.SmartAuth = null;
            this.BasicAuth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(uid + ":" + pwd));
            this.AuthType = FHIRAuthType.Basic;
            ShouldSave?.Invoke(this, new EventArgs());
        }

        public void ClearAuth()
        {
            this.SmartAuth = null;
            this.BasicAuth = null;
            this.AuthType = FHIRAuthType.None;
            ShouldSave?.Invoke(this, new EventArgs());
        }


        private string HexConverter(System.Drawing.Color c)
        {
            return String.Format("#{0:X6}", c.ToArgb() & 0x00FFFFFF);
        }

        public async Task FetchConformanceAsync()
        {
            ConformanceEndpoint = Endpoint;
            SmartUris = new SmartUris(Endpoint);
            using (var client = new HttpClient())
            {
                var metaUri = new Uri(_endpoint, "metadata");

                using (var metaResponse = await client.GetAsync(metaUri))
                {
                    if (metaResponse.IsSuccessStatusCode)
                    {
                        var text = (await metaResponse.Content.ReadAsStringAsync()).TrimStart();

                        if (text.StartsWith("<"))
                        {
                        }
                        else if (text.StartsWith("{"))
                        {
                        }

                        if (text.StartsWith("<"))
                        {
                            ConformanceXml = XDocument.Parse(text);
                            var ns = ConformanceXml.Root.Name.Namespace;
                            var publisher = ConformanceXml.Root.Element(ns + "publisher");
                            ConformancePublisher = publisher?.Attribute("value")?.Value;
                            var security = ConformanceXml.Root.Element(ns + "rest")?.Element(ns + "security");
                            ConformanceSecurityService = security?.Element(ns + "service")?.Element(ns + "coding")?.Element(ns + "code")?.Attribute("value")?.Value;

                            if (ConformanceSecurityService?.ToLowerInvariant() == SmartAuth.SERVICE_CODE)
                            {
                                var extensions = security?.Descendants().Where(x => x.Name.LocalName == "extension");

                                Uri uri = null;
                                var ext = extensions.FirstOrDefault(x => x.Attribute("url")?.Value == "authorize");
                                var url = ext?.Descendants().FirstOrDefault(x => x.Name.LocalName == "valueUri")?.Attribute("value")?.Value;
                                if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.AuthorizeUri = uri;
                                ext = extensions.FirstOrDefault(x => x.Attribute("url")?.Value == "token");
                                url = ext?.Descendants().FirstOrDefault(x => x.Name.LocalName == "valueUri")?.Attribute("value")?.Value;
                                if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.TokenUri = uri;
                                ext = extensions.FirstOrDefault(x => x.Attribute("url")?.Value == "register");
                                url = ext?.Descendants().FirstOrDefault(x => x.Name.LocalName == "valueUri")?.Attribute("value")?.Value;
                                if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.RegisterUri = uri;
                            }
                        }
                        else if (text.StartsWith("{"))
                        {
                            ConformanceJson = JObject.Parse(text);
                            ConformancePublisher = (string)ConformanceJson["publisher"];
                            var security = ConformanceJson["rest"][0]["security"];
                            ConformanceSecurityService = (string)security["service"][0]["coding"][0]["code"];
                            if (ConformanceSecurityService?.ToLowerInvariant() == SmartAuth.SERVICE_CODE)
                            {
                                Uri uri = null;
                                var token = security.SelectToken("$..extension[?(@.url == 'authorize')]");
                                var url = token?.Value<string>("valueUri");
                                if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.AuthorizeUri = uri;
                                token = security.SelectToken("$..extension[?(@.url == 'token')]");
                                url = token?.Value<string>("valueUri");
                                if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.TokenUri = uri;
                                token = security.SelectToken("$..extension[?(@.url == 'register')]");
                                url = token?.Value<string>("valueUri");
                                if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.RegisterUri = uri;
                            }
                        }
                    }

                    if (string.IsNullOrWhiteSpace(Name))
                    {
                        if (HasConformance && !string.IsNullOrWhiteSpace(ConformancePublisher))
                        {
                            Name = ConformancePublisher;
                        }
                        else
                        {
                            Name = _endpoint.Host;
                        }
                    }

                }
            }
        }
    }
}
