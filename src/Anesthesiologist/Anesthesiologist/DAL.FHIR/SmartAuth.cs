﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Xamarin.Forms;

namespace Anesthesiologist.DAL.FHIR
{
    public class SmartUris
    {
        public Uri BaseUri { get; set; }
        public Uri RegisterUri { get; set; }
        public Uri AuthorizeUri { get; set; }
        public Uri TokenUri { get; set; }
        public bool CanAuthenticate
        {
            get
            {
                return BaseUri != null &&
                       AuthorizeUri != null &&
                       TokenUri != null;
            }
        }
        public bool CanRegister
        {
            get
            {
                return CanAuthenticate && RegisterUri != null;
            }
        }

        public SmartUris() { } // required for JSON serialization

        public SmartUris(string baseUrl)
        {
            Uri uri = null;
            if (Uri.TryCreate(baseUrl, UriKind.Absolute, out uri))
                BaseUri = uri;
        }

        public SmartUris(Uri baseUri)
        {
            BaseUri = baseUri;
        }
    }

    public class SmartAuth
    {
        public const string SERVICE_SYSTEM = "http://hl7.org/fhir/restful-security-service";
        public const string SERVICE_CODE = "smart-on-fhir";


        private SmartUris _smartUris = new SmartUris("");

        public SmartUris SmartUris
        {
            get { return _smartUris; }
            set { _smartUris = value; }
        }



        private string _appName;

        public string AppName
        {
            get { return _appName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException("Must set AppName to a valid value");
                }

                _appName = value;
                //ShouldSave?.Invoke(this, new EventArgs());
            }
        }


        private string _scope;

        public string Scope
        {
            get { return _scope; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException("Must set Scope to a valid value");
                }

                _scope = value;
                //ShouldSave?.Invoke(this, new EventArgs());
            }
        }


        private Uri _redirectUri;

        public Uri RedirectUri
        {
            get { return _redirectUri; }
            set
            {
                _redirectUri = value;
                //ShouldSave?.Invoke(this, new EventArgs());
            }
        }


        private Uri _logoUri;

        public Uri LogoUri
        {
            get { return _logoUri; }
            set
            {
                _logoUri = value;
                //ShouldSave?.Invoke(this, new EventArgs());
            }
        }


        private string _clientId;

        public string ClientId
        {
            get { return _clientId; }
            set
            {
                _clientId = value;
                //ShouldSave?.Invoke(this, new EventArgs());
            }
        }

        private string _token;

        public string Token
        {
            get { return _token; }
            set
            {
                _token = value;
                //ShouldSave?.Invoke(this, new EventArgs());
            }
        }

        private DateTime _tokenExpiration;

        public DateTime TokenExpiration
        {
            get { return _tokenExpiration; }
            set
            {
                _tokenExpiration = value;
                //ShouldSave?.Invoke(this, new EventArgs());
            }
        }

        private string _refreshToken;

        public string RefreshToken
        {
            get { return _refreshToken; }
            set
            {
                _refreshToken = value;
                //ShouldSave?.Invoke(this, new EventArgs());
            }
        }

        public bool HasClientId
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.ClientId);
            }
        }

        public bool HasToken
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.Token);
            }
        }

        public bool HasValidToken
        {
            get
            {
                return HasToken && !HasExpiredToken;
            }
        }

        public bool HasExpiredToken
        {
            get
            {
                return HasToken && DateTime.Now > this.TokenExpiration;
            }
        }

        public bool HasRefreshToken
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.RefreshToken);
            }
        }

        public event EventHandler ShouldSave;

        public SmartAuth() { } // required for JSON serialization

        public SmartAuth(SmartUris uris,  string appName, string scope, Uri redirectUri = null, Uri logoUri = null)
        {
            SmartUris = uris;
            AppName = appName;
            Scope = scope;
            RedirectUri = redirectUri ?? new Uri("http://127.0.0.1/");
            LogoUri = logoUri;
        }

        public async Task<bool> InitializeAsync(INavigation navigation = null)
        {
            if (!SmartUris.CanAuthenticate)
            {
                await UpdateMetadataAsync();
            }

            if (!HasClientId)
            {
                // No Client Id yet, so try to self-register
                if (!SmartUris.CanRegister)
                {
                    throw new InvalidOperationException("No ClientId specified and no way to self-register");
                }
                // Now try to register the client
                await RegisterClient();
            }

            if (HasClientId && HasExpiredToken && HasRefreshToken)
            {
                // Try to refresh the token
                await RefreshTokenAsync();
            }

            if (HasClientId && !HasValidToken && navigation != null)
            {
                // Gotta log in
                await AuthenticateAsync(navigation);
            }

            return HasValidToken;
        }

        public async Task AuthenticateAsync(INavigation navigation)
        {
            // TODO (LOW): first try to authenticate with no GUI??? Not really an issue now that
            //             refresh tokens work, but may still be a nice-to-have for systems that
            //             don't support them.
            // need access to cookies
            // https://forums.xamarin.com/discussion/41039/setting-cookies-in-a-webview (last)
            // http://blog.wislon.io/posts/2016/01/24/xamforms-quick-and-dirty-cookies-access/

            var page = new LoginPage();
            page.Auth = this;
            await navigation.PushAsync(page);
            await page.PageClosedTask; // Bizarro trick to make a modal blocking in Xamarin
            if (page.LastException != null)
            {
                throw page.LastException;
            }
        }

        public async Task<bool> RegisterClient()
        {
            if (!SmartUris.CanRegister)
            {
                throw new InvalidOperationException("Cannot register client with no RegisterUri specified");
            }

            using (var client = new HttpClient())
            {
                var reg = new OAuthDynamicClientRegistrationRequest()
                {
                    client_name = this.AppName,
                    redirect_uris = new List<string> { this.RedirectUri.ToString() },
                    logo_uri = this.LogoUri?.ToString() ?? "",
                    scope = this.Scope
                };
                var regText = JsonConvert.SerializeObject(reg);

                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, SmartUris.RegisterUri);
                msg.Content = new StringContent(regText, Encoding.UTF8, "application/json");
                msg.Headers.Accept.Clear();
                msg.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                using (var regResponse = await client.SendAsync(msg))
                {
                    if (regResponse.IsSuccessStatusCode)
                    {
                        var text = await regResponse.Content.ReadAsStringAsync();
                        var jobj = JObject.Parse(text);

                        _clientId = jobj["client_id"]?.ToString();

                        ShouldSave?.Invoke(this, new EventArgs());

                        return this.HasClientId;
                    }
                }
            }
            return false;
        }

        public async Task UpdateMetadataAsync()
        {
            using (var client = new HttpClient())
            {
                var metaUri = SmartUris.BaseUri + "metadata";

                using (var metaResponse = await client.GetAsync(metaUri))
                {
                    if (metaResponse.IsSuccessStatusCode)
                    {
                        var text = (await metaResponse.Content.ReadAsStringAsync()).TrimStart();

                        if (text.StartsWith("<"))
                        {
                            var xdoc = XDocument.Parse(text);
                            var extensions = xdoc.Root.Descendants().Where(x => x.Name.LocalName == "extension");

                            Uri uri = null;
                            var ext = extensions.FirstOrDefault(x => x.Attribute("url")?.Value == "authorize");
                            var url = ext?.Descendants().FirstOrDefault(x => x.Name.LocalName == "valueUri")?.Attribute("value")?.Value;
                            if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.AuthorizeUri = uri;
                            ext = extensions.FirstOrDefault(x => x.Attribute("url")?.Value == "token");
                            url = ext?.Descendants().FirstOrDefault(x => x.Name.LocalName == "valueUri")?.Attribute("value")?.Value;
                            if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.TokenUri = uri;
                            ext = extensions.FirstOrDefault(x => x.Attribute("url")?.Value == "register");
                            url = ext?.Descendants().FirstOrDefault(x => x.Name.LocalName == "valueUri")?.Attribute("value")?.Value;
                            if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.RegisterUri = uri;

                            ShouldSave?.Invoke(this, new EventArgs());
                        }
                        else if (text.StartsWith("{"))
                        {
                            var jobj = JObject.Parse(text);

                            Uri uri = null;
                            var token = jobj.SelectToken("$..extension[?(@.url == 'authorize')]");
                            var url = token?.Value<string>("valueUri");
                            if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.AuthorizeUri = uri;
                            token = jobj.SelectToken("$..extension[?(@.url == 'token')]");
                            url = token?.Value<string>("valueUri");
                            if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.TokenUri = uri;
                            token = jobj.SelectToken("$..extension[?(@.url == 'register')]");
                            url = token?.Value<string>("valueUri");
                            if (Uri.TryCreate(url, UriKind.Absolute, out uri)) SmartUris.RegisterUri = uri;

                            ShouldSave?.Invoke(this, new EventArgs());
                        }
                    }
                }
            }
        }

        public async Task RefreshTokenAsync()
        {
            if (!this.HasRefreshToken)
            {
                return;
            }

            using (var client = new HttpClient())
            {
                var body = $"grant_type=refresh_token&client_id={this.ClientId}&refresh_token={this.RefreshToken}";
                body = Uri.EscapeUriString(body);

                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, SmartUris.TokenUri);
                msg.Content = new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded");
                msg.Headers.Accept.Clear();
                msg.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                using (var response = await client.SendAsync(msg))
                {
                    var content = await response.Content.ReadAsStringAsync();
                    await _GetTokenFromResponseAsync(response);
                }
            }
        }

        private async Task _ExchangeCodeForTokenAsync(string code)
        {
            using (var client = new HttpClient())
            {
                var body = $"grant_type=authorization_code&client_id={this.ClientId}&code={code}&redirect_url={this.RedirectUri}";
                body = Uri.EscapeUriString(body);

                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, SmartUris.TokenUri);
                msg.Content = new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded");
                msg.Headers.Accept.Clear();
                msg.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                using (var response = await client.SendAsync(msg))
                {
                    await _GetTokenFromResponseAsync(response);
                }
            }
        }

        private async Task _GetTokenFromResponseAsync(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                var text = await response.Content.ReadAsStringAsync();
                var jobj = JObject.Parse(text);

                _token = jobj["access_token"]?.ToString();
                _refreshToken = jobj["refresh_token"]?.ToString();
                int expiresIn = 0;
                int.TryParse(jobj["expires_in"]?.ToString(), out expiresIn);
                _tokenExpiration = DateTime.Now.AddSeconds(expiresIn);

                ShouldSave?.Invoke(this, new EventArgs());
            }

        }



        // Borrowed from:
        // http://stackoverflow.com/questions/20268544/portable-class-library-pcl-version-of-httputility-parsequerystring
        public static Dictionary<string, string> ParseQueryString(Uri uri)
        {
            var query = uri.Query.Substring(uri.Query.IndexOf('?') + 1); // +1 for skipping '?'
            var pairs = query.Split('&');
            return pairs
                .Select(o => o.Split('='))
                .Where(items => items.Count() == 2)
                .ToDictionary(pair => Uri.UnescapeDataString(pair[0]),
                    pair => Uri.UnescapeDataString(pair[1]));
        }



        ///////////////////////////////////////////////////////////////////////
        #region LOGIN PAGE
        ///////////////////////////////////////////////////////////////////////


        private class LoginPage : ContentPage
        {
            public SmartAuth Auth { get; set; }

            public Task PageClosedTask { get { return tcs.Task; } }

            private TaskCompletionSource<bool> tcs { get; set; } = new TaskCompletionSource<bool>();

            public Exception LastException { get; set; }


            private Grid _busy;
            private WebView _browser;

            private string _state;

            public LoginPage()
            {
                this.Title = "FHIR Authorization";

                _browser = new WebView
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                _browser.Navigating += async (s, e) =>
                {
                    _busy.IsVisible = true;

                    if (!e.Url.StartsWith(Auth.RedirectUri.ToString()))
                    {
                        return;
                    }

                    Uri uri = new Uri(e.Url);

                    var q = ParseQueryString(uri);
                    try
                    {

                        if (q.ContainsKey("error"))
                        {
                            throw new UnauthorizedAccessException(q["error"]);
                        }

                        if (!q.ContainsKey("code"))
                        {
                            throw new UnauthorizedAccessException("Unexpeced response from server.");
                        }

                        var code = q["code"];

                        e.Cancel = true;

                        var testState = q["state"];
                        if (_state != testState)
                        {
                            throw new UnauthorizedAccessException("State values do not match");
                        }

                        await Auth._ExchangeCodeForTokenAsync(code);
                    }
                    catch (Exception ex)
                    {
                        e.Cancel = true;
                        this.LastException = ex;
                    }
                    finally
                    {
                        if (Navigation.NavigationStack.LastOrDefault() == this)
                        {
                            await Navigation.PopAsync();
                        }
                    }
                };
                _browser.Navigated += (s, e) =>
                {
                    _busy.IsVisible = false;
                };

                _busy = new Grid
                {
                    IsVisible = true,
                    RowDefinitions =
                    {
                        new RowDefinition { Height = GridLength.Auto }
                    },
                    ColumnDefinitions =
                    {
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition { Width = GridLength.Auto }
                    }
                };
                _busy.Children.Add(new Label
                {
                    Text = "Loading...",
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    VerticalTextAlignment = TextAlignment.Center,
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                    FontAttributes = FontAttributes.Bold
                }, 0, 0);
                _busy.Children.Add(new ActivityIndicator
                {
                    IsRunning = true
                }, 1, 0);

                this.Content = new StackLayout
                {
                    Children =
                    {
                        _busy,
                        _browser
                    }
                };

            }

            protected override async void OnDisappearing()
            {
                base.OnDisappearing();
                try
                {
                    if (Navigation.NavigationStack.LastOrDefault() == this)
                    {
                        await Navigation.PopAsync();
                    }
                }
                catch { }
                tcs.SetResult(true);
            }

            protected override void OnAppearing()
            {
                base.OnAppearing();
                _state = Guid.NewGuid().ToString();
                var scope = Auth.Scope.Replace(" ", "+");
                _browser.Source = $"{Auth.SmartUris.AuthorizeUri}?response_type=code&client_id={Auth.ClientId}&redirect_url={Auth.RedirectUri}&scope={scope}&state={_state}&aud={Auth.SmartUris.BaseUri}".Replace(" ", "+");
                //  Uri.EscapeUriString($"{Auth.AuthorizeUri}?response_type=code&client_id={Auth.ClientId}&redirect_url={Auth.RedirectUri}&scope={ Auth.Scope}&state={_state}&aud={Auth.BaseUri}");
            }

        }

        ///////////////////////////////////////////////////////////////////////
        #endregion
        ///////////////////////////////////////////////////////////////////////

    }
}
