﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anesthesiologist.DAL.FHIR
{
    class OAuthDynamicClientRegistrationRequest
    {
        public string client_name { get; set; } = "";
        public List<string> redirect_uris { get; set; } = new List<string>();
        public string token_endpoint_auth_method { get; set; } = "none";
        public List<string> grant_types { get; set; } = new List<string>() { "authorization_code", "refresh_token" };
        public string initiate_login_uri { get; set; } = "";
        public string logo_uri { get; set; } = "";
        public string scope { get; set; } = "";
    }
}
