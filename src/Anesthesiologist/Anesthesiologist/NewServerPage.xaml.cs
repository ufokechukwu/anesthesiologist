﻿using Anesthesiologist.DAL;
using Anesthesiologist.DAL.FHIR;
using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Anesthesiologist
{
	public partial class NewServerPage : ContentPage
	{
        const string APP_NAME = "Anesthesiologist App";
        const string SCOPE = "user/*.read offline_access";

        private FHIRDataProviderConfig _fhirConfig = null;

        public NewServerPage()
		{
			InitializeComponent();
			Title = "New Server";

            serverTypeRow.Height = 0; // TODO (LOW): Remove when supporting multiple server types
            serverType.SelectedIndexChanged += ServerType_SelectedIndexChanged;

            serverUrlAcceptButton.Clicked += ServerUrlAcceptButton_Clicked;

            fhirAuthenticationType.SelectedIndexChanged += FhirAuthenticationType_SelectedIndexChanged;
            fhirSmartAutoRegister.Toggled += FhirSmartAutoRegister_Toggled;

            saveButton.Clicked += SaveButton_Clicked;

            serverType.SelectedIndex = 0;
            fhirAuthenticationType.SelectedIndex = 0;

            serverNameRow.Height = 0;
            fhirAuthenticationTypeRow.Height = 0;
            fhirSmartAutoRegisterRow.Height = 0;
            fhirSmartClientIdRow.Height = 0;
            fhirSmartRedirectUrlRow.Height = 0;
            saveButton.IsVisible = false;
		}

        private void ServerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // TODO (LOW): In the future support multiple server types
        }

        private async void ServerUrlAcceptButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                var urlString = serverUrl.Text.EndsWith("/") ? serverUrl.Text : serverUrl.Text + "/";

                Uri uri = null;
                if (!Uri.TryCreate(urlString, UriKind.Absolute, out uri))
                {
                    await DisplayAlert("Error Setting URL", "Please enter a valid URL", "Close");
                    return;
                }

                serverUrl.IsEnabled = false;
                serverUrlAcceptButton.IsEnabled = false;

                _fhirConfig = new FHIRDataProviderConfig
                {
                    Endpoint = uri
                };

                await _fhirConfig.FetchConformanceAsync();
                if (!_fhirConfig.HasConformance)
                {
                    await DisplayAlert("Error Setting URL", "The specified URL does not appear to point to a valid FHIR server", "Close");
                    serverUrl.IsEnabled = true;
                    serverUrlAcceptButton.IsEnabled = true;
                    return;
                }

                serverUrlAcceptButton.IsVisible = false;

                serverNameRow.Height = 60;
                fhirAuthenticationTypeRow.Height = 60;

                saveButton.IsVisible = true;

                if (string.IsNullOrWhiteSpace(serverName.Text))
                {
                    serverName.Text = _fhirConfig.ConformancePublisher;
                }

                if (_fhirConfig.ConformanceSecurityService?.ToLowerInvariant() == "smart-on-fhir")
                {
                    fhirAuthenticationType.SelectedIndex = 1;
                    fhirAuthenticationType.IsEnabled = true;

                    if (_fhirConfig.SupportsAutoRegistration)
                    {
                        fhirSmartAutoRegister.IsToggled = true;
                        fhirSmartAutoRegister.IsEnabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error Setting URL", ex.Message, "Close");
                serverUrl.IsEnabled = true;
                serverUrlAcceptButton.IsEnabled = true;
            }
        }

        private void FhirAuthenticationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            fhirSmartAutoRegisterRow.Height = fhirAuthenticationType.SelectedIndex == 1 ? 60 : 0;
            FhirSmartAutoRegister_Toggled(sender, null);
        }

        private void FhirSmartAutoRegister_Toggled(object sender, ToggledEventArgs e)
        {
            var height = fhirAuthenticationType.SelectedIndex == 1 && !fhirSmartAutoRegister.IsToggled ? 60 : 0;
            fhirSmartClientIdRow.Height = height;
            fhirSmartRedirectUrlRow.Height = height;
        }

        private async void SaveButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                IDataProviderConfig config = null;

                if (serverType.SelectedIndex == 0) // FHIR ... no other server type at the moment
                {
                    var fhirConfig = new FHIRDataProviderConfig();
                    config = fhirConfig;

                    if (string.IsNullOrWhiteSpace(serverName.Text))
                    {
                        await DisplayAlert("ERROR", "Must supply a Server Name", "Close");
                        return;
                    }
                    fhirConfig.Name = serverName.Text;

                    Uri endpoint = null;
                    if (Uri.TryCreate(serverUrl.Text, UriKind.Absolute, out endpoint))
                    {
                        fhirConfig.Endpoint = endpoint;
                    }
                    else
                    {
                        await DisplayAlert("ERROR", "Server URI must be a valid URL", "Close");
                        return;
                    }

                    if (fhirAuthenticationType.SelectedIndex == 1) // SMART
                    {
                        var auth = new SmartAuth(new SmartUris(fhirConfig.Endpoint.OriginalString), APP_NAME, SCOPE);
                        await auth.UpdateMetadataAsync();

                        if (!fhirSmartAutoRegister.IsToggled)
                        {
                            if (string.IsNullOrWhiteSpace(fhirSmartClientId.Text))
                            {
                                await DisplayAlert("ERROR", "Must supply a client id", "Close");
                                return;
                            }
                            auth.ClientId = fhirSmartClientId.Text;

                            Uri redirectUri = null;
                            if (Uri.TryCreate(fhirSmartRedirectUrl.Text, UriKind.Absolute, out redirectUri))
                            {
                                auth.RedirectUri = redirectUri;
                            }
                            else
                            {
                                await DisplayAlert("ERROR", "Redirect URI must be a valid URL", "Close");
                                return;
                            }
                        }

                        fhirConfig.SetAuth(auth);

                        await IDataStorage.AddFhirSession(fhirConfig.UUID);
                    }
                }

                if (config != null)
                {
                    await DALFactory.AddConfigurationAsync(config);
                    await Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("ERROR", ex.Message, "Close");
            }
        }
    }

    //public class NewServerData : BindableObject
    //{
    //    public static readonly BindableProperty ServerNameProperty =
    //        BindableProperty.Create(nameof(ServerName), typeof(string), typeof(NewServerData));

    //    public string ServerName
    //    {
    //        get { return (string)GetValue(ServerNameProperty); }
    //        set { SetValue(ServerNameProperty, value); }
    //    }

    //    public static readonly BindableProperty ServerTypeIndexProperty =
    //        BindableProperty.Create(nameof(ServerTypeIndex), typeof(int), typeof(NewServerData));

    //    public int ServerTypeIndex
    //    {
    //        get { return (int)GetValue(ServerTypeIndexProperty); }
    //        set
    //        {
    //            SetValue(ServerTypeIndexProperty, value);
    //            IsFhir = value == 0;
    //        }
    //    }

    //    public static readonly BindableProperty IsFhirProperty =
    //        BindableProperty.Create(nameof(IsFhir), typeof(bool), typeof(NewServerData));

    //    public bool IsFhir
    //    {
    //        get { return (bool)GetValue(IsFhirProperty); }
    //        set { SetValue(IsFhirProperty, value); }
    //    }

    //    public static readonly BindableProperty FhirServerUrlProperty =
    //        BindableProperty.Create(nameof(FhirServerUrl), typeof(string), typeof(NewServerData));

    //    public string FhirServerUrl
    //    {
    //        get { return (string)GetValue(FhirServerUrlProperty); }
    //        set { SetValue(FhirServerUrlProperty, value); }
    //    }

    //    public static readonly BindableProperty FhirAuthenticationTypeIndexProperty =
    //        BindableProperty.Create(nameof(FhirAuthenticationTypeIndex), typeof(int), typeof(NewServerData));

    //    public int FhirAuthenticationTypeIndex
    //    {
    //        get { return (int)GetValue(FhirAuthenticationTypeIndexProperty); }
    //        set
    //        {
    //            SetValue(FhirAuthenticationTypeIndexProperty, value);
    //            IsFhirSmart = value == 0;
    //        }
    //    }

    //    public static readonly BindableProperty IsFhirSmartProperty =
    //        BindableProperty.Create(nameof(IsFhirSmart), typeof(bool), typeof(NewServerData));

    //    public bool IsFhirSmart
    //    {
    //        get { return (bool)GetValue(IsFhirSmartProperty); }
    //        set { SetValue(IsFhirSmartProperty, value); }
    //    }

    //    public static readonly BindableProperty IsFhirSmartAutoRegisterProperty =
    //        BindableProperty.Create(nameof(IsFhirSmartAutoRegister), typeof(bool), typeof(NewServerData));

    //    public bool IsFhirSmartAutoRegister
    //    {
    //        get { return (bool)GetValue(IsFhirSmartAutoRegisterProperty); }
    //        set { SetValue(IsFhirSmartAutoRegisterProperty, value); }
    //    }

    //    public static readonly BindableProperty FhirSmartClientIdProperty =
    //        BindableProperty.Create(nameof(FhirSmartClientId), typeof(string), typeof(NewServerData));

    //    public string FhirSmartClientId
    //    {
    //        get { return (string)GetValue(FhirSmartClientIdProperty); }
    //        set { SetValue(FhirSmartClientIdProperty, value); }
    //    }


    //    public static readonly BindableProperty FhirSmartRedirectUrlProperty =
    //        BindableProperty.Create(nameof(FhirSmartRedirectUrl), typeof(string), typeof(NewServerData));

    //    public string FhirSmartRedirectUrl
    //    {
    //        get { return (string)GetValue(FhirSmartRedirectUrlProperty); }
    //        set { SetValue(FhirSmartRedirectUrlProperty, value); }
    //    }
    //}

}
