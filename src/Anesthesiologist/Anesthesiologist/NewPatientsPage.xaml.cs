﻿using Anesthesiologist.DAL.FHIR;
using System;
using System.Collections.Generic;


using Xamarin.Forms;

namespace Anesthesiologist
{
	public partial class NewPatientsPage : ContentPage
	{
		string[] searchList;
        FHIRSearchType fhirSearchType;

		public NewPatientsPage()
		{
			InitializeComponent();

			Title = "New Patient";

            fhirSearchType = FHIRSearchType.ID;

			searchList = new string[]
			{
				"Patient ID",
				"Patient Last Name",
				"Patient Given Name",
				"Patient Name"
			};
		}

		async void OnSelectCriteriaClicked(object sender, EventArgs args)
		{
			var alert = await DisplayActionSheet("Select A Search Criteria", "Cancel", null, searchList);

			switch (alert)
			{
				case "Patient ID":
					SetSearchCriteriaTo(FHIRSearchType.ID);
					break;
				case "Patient Last Name":
					SetSearchCriteriaTo(FHIRSearchType.LastName);
					break;
				case "Patient Given Name":
					SetSearchCriteriaTo(FHIRSearchType.GivenName);
					break;
				case "Patient Name":
					SetSearchCriteriaTo(FHIRSearchType.Name);
					break;
				default:
					break;
			}
		}

		async void OnSearchClicked(object sender, EventArgs args)
		{
			//Button button = (Button)sender;
			//await DisplayAlert("Clicked!","The button labeled '" + button.Text + "' has been clicked", "OK");


			if (!String.IsNullOrWhiteSpace(searchInput.Text) && (searchInput.Text.Length > 1))
			{
				var detailPage = new NewPatientSearchListPage(fhirSearchType, searchInput.Text);
				await Navigation.PushAsync(detailPage);
			}
			else {
				await DisplayAlert("Alert!", "Please input a valid value", "OK");

			}
	
		}

		private void SetSearchCriteriaTo(FHIRSearchType searchCriteria)
		{
			searchType.Text = "Search By : " + searchList[(int)searchCriteria];
			searchInput.Placeholder = searchList[(int)searchCriteria];
			searchInput.Text = "";
            fhirSearchType = searchCriteria;
		}
	}
}
