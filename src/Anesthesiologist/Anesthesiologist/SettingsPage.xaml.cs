﻿using Anesthesiologist.DAL;
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Anesthesiologist
{
    public partial class SettingsPage : ContentPage
    {
        public SettingsPage()
        {
            InitializeComponent();

            Title = "App Settings";

            ToolbarItems.Add(
                new ToolbarItem("Add Server", "circle_white.png", async () =>
                {
                    var newPage = new NewServerPage();
                    await Navigation.PushAsync(newPage);
                })
            );

            ServerListXAML.ItemSelected += OnItemSelected;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            RefreshServerList();
        }

        private async void RefreshServerList()
        {
            ServerListXAML.ItemsSource = null;
            ServerListXAML.ItemsSource = await DALFactory.GetAllConfigurationsAsync();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                var server = e.SelectedItem as IDataProviderConfig;
                ServerListXAML.SelectedItem = null;
                if (server != null)
                {
                    var detailPage = new ServerInfoPage(server);
                    await Navigation.PushAsync(detailPage);
                }
            }
        }

        void OnActivate(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            var config = mi.CommandParameter as IDataProviderConfig;
            if (config == null) return;

            DALFactory.SetActiveConfiguration(config);
            RefreshServerList();
        }

        async void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            var config = mi.CommandParameter as IDataProviderConfig;
            if (config == null) return;

            if (await DisplayAlert("Confirm Delete", $"Are you sure you wish to delete {config.Name}?", "Yes", "No"))
            {
                await DALFactory.RemoveConfigurationAsync(config);
                RefreshServerList();
            }
        }
    }
}